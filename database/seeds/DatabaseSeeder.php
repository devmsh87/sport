<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    public function run() {

        $this->call([
            WorldCupTableSeeder::class
        ]);

        return;

        // create associations
        $associations = factory(\App\Association::class, 3)->create();

        // and teams
        $teams = factory(\App\Team::class, 20)->create();

        // then attach leagues with every association
        // each league linked to session and random teams
        $associations->each(function ($association) use ($teams) {
            factory(\App\League::class, 3)->create([
                'association_id' => $association->id
            ])->each(function ($league) use ($teams) {
                factory(\App\Season::class)->create([
                    'league_id' => $league->id
                ]);

                foreach (range(1, 5) as $id) {
                    $league->season->teams()->attach($teams->random()->id);

                }
            });
        });

        // then add matches for each season from yesterday to 30 days in future
        $seasons = \App\Season::all();

        foreach ($seasons as $season) {
            $teams = $season->league->season->teams;
            foreach (range(-1, 30) as $days) {
                foreach (range(1,3) as $i) {
                    factory(\App\MatchTeam::class)->create([
                        'first_team_id' => $teams->random()->id,
                        'second_team_id' => $teams->random()->id,
                        'match_id' => factory(\App\Match::class)->create([
                            'started_at' => \Carbon\Carbon::now()->addDays($days),
                            'season_id' => $season->id,
                        ])->id
                    ]);
                }
            }
        }
    }
}
