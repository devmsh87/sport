<?php

use App\Match;
use App\MatchTeam;
use App\Season;
use App\Stage;
use App\Team;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

/**
 * @property Team russia
 * @property Team saudi_arabia
 * @property Team egypt
 * @property Team uruguay
 * @property Team portugal
 * @property Team spain
 * @property Team morocco
 * @property Team iran
 * @property Team france
 * @property Team australia
 * @property Team peru
 * @property Team denmark
 * @property Team argentina
 * @property Team iceland
 * @property Team croatia
 * @property Team nigeria
 * @property Team brazil
 * @property Team switzerland
 * @property Team costa_rica
 * @property Team serbia
 * @property Team germany
 * @property Team mexico
 * @property Team sweden
 * @property Team $south_korea
 * @property Team belgium
 * @property Team panama
 * @property Team tunisia
 * @property Team $england
 * @property Team poland
 * @property Team senegal
 * @property Team colombia
 * @property Team japan
 */
class WorldCupTableSeeder extends Seeder
{
    const STAGE_ROUND_OF_16_MAP = [
        [["C1", "D2", "30-6 5:00 pm"], "W50"],
        [["A1", "B2", "30-6 9:00 pm"], "W49"],

        [["B1", "A2", "1-7 5:00 pm"], "W51"],
        [["D1", "C2", "1-7 9:00 pm"], "W52"],

        [["E1", "F2", "2-7 5:00 pm"], "W53"],
        [["G1", "H2", "2-7 9:00 pm"], "W54"],

        [["F1", "E2", "3-7 5:00 pm"], "W55"],
        [["H1", "G2", "3-7 9:00 pm"], "W56"],
    ];

    const STAGE_QUARTER_FINAL_MAP = [
        [["W49", "W50", "6-7 5:00 pm"], "W57"],
        [["W53", "W54", "6-7 9:00 pm"], "W58"],

        [["W55", "W56", "7-7 5:00 pm"], "W60"],
        [["W51", "W52", "7-7 9:00 pm"], "W59"],
    ];

    const STAGE_SEMI_FINAL_MAP = [
        [["W57", "W58", "10-7 9:00 pm"], "W61", "L61"],
        [["W59", "W60", "11-7 9:00 pm"], "W62", "L62"],
    ];

    const STAGE_THIRD_MATCH = [["L61", "L62", "14-7 5:00 pm"]];

    const STAGE_FINAL_MATCH = [["W61", "W62", "15-7 6:00 pm"]];

    public function run()
    {
        $association = factory(\App\Association::class)->create([
            'name' => 'FIFA'
        ]);

        $league = factory(\App\League::class)->create([
            "name" => "World Cup",
            "association_id" => $association->id
        ]);

        $season = factory(Season::class)->create([
            "name" => "World Cup 2018",
            "league_id" => $league->id
        ]);

        $groups = range('A', 'H');
        $groupTeams = [
            "A" => [
                $this->russia = \App\Team::create([
                    "name" => "Russia",
                    "shortcut" => "RU",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/russia.gif"
                ]),
                $this->saudi_arabia = \App\Team::create([
                    "name" => "Saudi Arabia",
                    "shortcut" => "SA",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/saudi_arabia.gif"
                ]),
                $this->egypt = \App\Team::create([
                    "name" => "Egypt",
                    "shortcut" => "EG",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/egypt.gif"
                ]),
                $this->uruguay = \App\Team::create([
                    "name" => "Uruguay",
                    "shortcut" => "UY",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/uruguay.gif"
                ]),
            ],
            "B" => [
                $this->portugal = \App\Team::create([
                    "name" => "Portugal",
                    "shortcut" => "PT",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/portugal.gif"
                ]),
                $this->spain = \App\Team::create([
                    "name" => "Spain",
                    "shortcut" => "ES",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/spain.gif"
                ]),
                $this->morocco = \App\Team::create([
                    "name" => "Morocco",
                    "shortcut" => "MA",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/morocco.gif"
                ]),
                $this->iran = \App\Team::create([
                    "name" => "Iran",
                    "shortcut" => "IR",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/iran.gif"
                ]),
            ],
            "C" => [
                $this->france = \App\Team::create([
                    "name" => "France",
                    "shortcut" => "FR",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/france.gif"
                ]),
                $this->australia = \App\Team::create([
                    "name" => "Australia",
                    "shortcut" => "AU",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/australia.gif"
                ]),
                $this->peru = \App\Team::create([
                    "name" => "Peru",
                    "shortcut" => "PE",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/peru.gif"
                ]),
                $this->denmark = \App\Team::create([
                    "name" => "Denmark",
                    "shortcut" => "DK",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/denmark.gif"
                ]),
            ],
            "D" => [
                $this->argentina = \App\Team::create([
                    "name" => "Argentina",
                    "shortcut" => "AR",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/argentina.gif"
                ]),
                $this->iceland = \App\Team::create([
                    "name" => "Iceland",
                    "shortcut" => "IS",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/iceland.gif"
                ]),
                $this->croatia = \App\Team::create([
                    "name" => "Croatia",
                    "shortcut" => "HR",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/croatia.gif"
                ]),
                $this->nigeria = \App\Team::create([
                    "name" => "Nigeria",
                    "shortcut" => "NG",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/nigeria.gif"
                ]),
            ],
            "E" => [
                $this->brazil = \App\Team::create([
                    "name" => "Brazil",
                    "shortcut" => "BR",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/brazil.gif"
                ]),
                $this->switzerland = \App\Team::create([
                    "name" => "Switzerland",
                    "shortcut" => "CH",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/switzerland.gif"
                ]),
                $this->costa_rica = \App\Team::create([
                    "name" => "Costa Rica",
                    "shortcut" => "CR",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/costa_rica.gif"
                ]),
                $this->serbia = \App\Team::create([
                    "name" => "Serbia",
                    "shortcut" => "RS",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/serbia.gif"
                ]),
            ],
            "F" => [
                $this->germany = \App\Team::create([
                    "name" => "Germany",
                    "shortcut" => "DE",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/germany.gif"
                ]),
                $this->mexico = \App\Team::create([
                    "name" => "Mexico",
                    "shortcut" => "MX",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/mexico.gif"
                ]),
                $this->sweden = \App\Team::create([
                    "name" => "Sweden",
                    "shortcut" => "SE",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/sweden.gif"
                ]),
                $this->south_korea = \App\Team::create([
                    "name" => "South Korea",
                    "shortcut" => "KR",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/korea_south.gif"
                ]),
            ],
            "G" => [
                $this->belgium = \App\Team::create([
                    "name" => "Belgium",
                    "shortcut" => "BE",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/belgium.gif"
                ]),
                $this->panama = \App\Team::create([
                    "name" => "Panama",
                    "shortcut" => "PA",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/panama.gif"
                ]),
                $this->tunisia = \App\Team::create([
                    "name" => "Tunisia",
                    "shortcut" => "TN",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/tunisia.gif"
                ]),
                $this->england = \App\Team::create([
                    "name" => "England",
                    "shortcut" => "UK",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/united_kingdom.gif"
                ]),
            ],
            "H" => [
                $this->poland = \App\Team::create([
                    "name" => "Poland",
                    "shortcut" => "BE",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/poland.gif"
                ]),
                $this->senegal = \App\Team::create([
                    "name" => "Senegal",
                    "shortcut" => "SN",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/senegal.gif"
                ]),
                $this->colombia = \App\Team::create([
                    "name" => "Colombia",
                    "shortcut" => "CO",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/colombia.gif"
                ]),
                $this->japan = \App\Team::create([
                    "name" => "Japan",
                    "shortcut" => "JP",
                    "logo" => "https://cdn.jsdelivr.net/npm/country-flags@0.0.2/flags/japan.gif"
                ]),
            ],
        ];

        foreach ($groups as $group) {
            $season->teams()->attach(collect($groupTeams[$group])->pluck('id'),[
                "group" => $group
            ]);
        }

        $groupStage = factory(Stage::class)->create([
            "name" => "Group Stage",
            "type" => Stage::STAGE_GROUPS,
            "season_id" => $season->id,
            "weight" => 500
        ]);

        $roundOf16Stage = factory(Stage::class)->create([
            "name" => "Round of 16",
            "type" => Stage::STAGE_16,
            "season_id" => $season->id,
            "weight" => 500
        ]);

        $quarterFinalStage = factory(Stage::class)->create([
            "name" => "Quarter Final",
            "type" => Stage::STAGE_8,
            "season_id" => $season->id,
            "weight" => 500
        ]);

        $semiFinalStage = factory(Stage::class)->create([
            "name" => "Semi Final",
            "type" => Stage::STAGE_4,
            "season_id" => $season->id,
            "weight" => 500
        ]);

        $thirdPlaceStage = factory(Stage::class)->create([
            "name" => "Third place play-off",
            "type" => Stage::STAGE_THIRD,
            "season_id" => $season->id,
            "weight" => 500
        ]);

        $finalStage = factory(Stage::class)->create([
            "name" => "Finals",
            "type" => Stage::STAGE_FINAL,
            "season_id" => $season->id,
            "weight" => 0
        ]);

        $this->createLabeledMatch(self::STAGE_FINAL_MATCH, $season, $finalStage);


        $this->createLabeledMatch(self::STAGE_THIRD_MATCH, $season, $thirdPlaceStage);

        foreach (self::STAGE_SEMI_FINAL_MAP as $match) {
            $this->createLabeledMatch($match, $season, $semiFinalStage);
        }

        foreach (self::STAGE_QUARTER_FINAL_MAP as $match) {
            $this->createLabeledMatch($match, $season, $quarterFinalStage);
        }

        foreach (self::STAGE_ROUND_OF_16_MAP as $match) {
            $this->createLabeledMatch($match, $season, $roundOf16Stage);
        }

        foreach ($this->groupStageMatches() as $match) {
            $this->createMatch($match, $season, $groupStage);
        }
    }

    public function createMatch($match, $season, $stage)
    {
        [$first, $second, $at] = $match;
        return factory(MatchTeam::class)->create([
            'first_team_id' => $first->id,
            'second_team_id' => $second->id,
            'match_id' => factory(Match::class)->create([
                'season_id' => $season->id,
                'stage_id' => $stage->id,
                'started_at' => Carbon::createFromFormat("d-m H:i a", $at)
            ])->id
        ])->match;
    }

    private function groupStageMatches()
    {
        return [
            [$this->russia, $this->saudi_arabia, "14-6 6:00 pm"],

            [$this->egypt, $this->uruguay, "15-6 3:00 pm"],
            [$this->morocco, $this->iran, "15-6 6:00 pm"],
            [$this->portugal, $this->spain, "15-6 9:00 pm"],

            [$this->france, $this->australia, "16-6 1:00 pm"],
            [$this->argentina, $this->iceland, "16-6 4:00 pm"],
            [$this->peru, $this->denmark, "16-6 7:00 pm"],
            [$this->croatia, $this->nigeria, "16-6 10:00 pm"],

            [$this->costa_rica, $this->serbia, "17-6 3:00 pm"],
            [$this->germany, $this->mexico, "17-6 6:00 pm"],
            [$this->brazil, $this->switzerland, "17-6 9:00 pm"],

            [$this->sweden, $this->south_korea, "18-6 3:00 pm"],
            [$this->belgium, $this->panama, "18-6 6:00 pm"],
            [$this->tunisia, $this->england, "18-6 9:00 pm"],

            [$this->colombia, $this->japan, "19-6 3:00 pm"],
            [$this->poland, $this->senegal, "19-6 6:00 pm"],
            [$this->russia, $this->egypt, "19-6 9:00 pm"],

            [$this->portugal, $this->morocco, "20-6 3:00 pm"],
            [$this->uruguay, $this->saudi_arabia, "20-6 6:00 pm"],
            [$this->iran, $this->spain, "20-6 9:00 pm"],

            [$this->denmark, $this->australia, "21-6 3:00 pm"],
            [$this->france, $this->peru, "21-6 6:00 pm"],
            [$this->argentina, $this->croatia, "21-6 9:00 pm"],

            [$this->brazil, $this->costa_rica, "22-6 3:00 pm"],
            [$this->nigeria, $this->iceland, "22-6 6:00 pm"],
            [$this->serbia, $this->switzerland, "22-6 9:00 pm"],

            [$this->belgium, $this->tunisia, "23-6 3:00 pm"],
            [$this->south_korea, $this->mexico, "23-6 6:00 pm"],
            [$this->germany, $this->sweden, "23-6 9:00 pm"],

            [$this->england, $this->panama, "24-6 3:00 pm"],
            [$this->japan, $this->senegal, "24-6 6:00 pm"],
            [$this->poland, $this->colombia, "24-6 9:00 pm"],

            [$this->saudi_arabia, $this->egypt, "25-6 5:00 pm"],
            [$this->uruguay, $this->russia, "25-6 5:00 pm"],
            [$this->iran, $this->portugal, "25-6 9:00 pm"],
            [$this->spain, $this->morocco, "25-6 9:00 pm"],

            [$this->australia, $this->peru, "26-6 5:00 pm"],
            [$this->denmark, $this->france, "26-6 5:00 pm"],
            [$this->nigeria, $this->argentina, "26-6 9:00 pm"],
            [$this->iceland, $this->croatia, "26-6 9:00 pm"],

            [$this->mexico, $this->sweden, "27-6 5:00 pm"],
            [$this->south_korea, $this->germany, "27-6 5:00 pm"],
            [$this->switzerland, $this->costa_rica, "27-6 9:00 pm"],
            [$this->serbia, $this->brazil, "27-6 9:00 pm"],

            [$this->senegal, $this->colombia, "28-6 5:00 pm"],
            [$this->japan, $this->poland, "28-6 5:00 pm"],
            [$this->england, $this->belgium, "28-6 9:00 pm"],
            [$this->panama, $this->tunisia, "28-6 9:00 pm"],
        ];
    }

    public function createLabeledMatch($match, $season, $stage, $data = null)
    {
        $winner = null;
        $loser = null;
        [[$first, $second, $at]] = $match;
        if (isset($match[1])) {
            $winner = $match[1];
        }
        if (isset($match[2])) {
            $loser = $match[2];
        }

        $match_id = Match::create([
            'season_id' => $season->id,
            'stage_id' => $stage->id,
            'started_at' => Carbon::createFromFormat("d-m H:i a", $at)
        ])->id;

        $matchTeam = factory(MatchTeam::class)->create([
            'first_team_label' => $first,
            'second_team_label' => $second,
            'first_team_id' => null,
            'second_team_id' => null,
            'match_id' => $match_id
        ]);

//        if ($winner) {
//            $winnerMatch = MatchTeam::where('first_team_label', $winner)->orWhere('second_team_label', $winner)->first();
//            $matchTeam->match->update([
//                'winner_to_id' => $winnerMatch->match_id,
//                'winner_to_position' => $winnerMatch->first_team_label == $winner? Match::POSITION_FIRST : Match::POSITION_SECOND,
//            ]);
//        }
//
//        if ($loser) {
//            $loserMatch = MatchTeam::where('first_team_label', $loser)->orWhere('second_team_label', $loser)->first();
//            $matchTeam->match->update([
//                'loser_to_id' => $loserMatch->match_id,
//                'loser_to_position' => $winnerMatch->first_team_label == $winner? Match::POSITION_FIRST : Match::POSITION_SECOND,
//            ]);
//        }

        return $matchTeam->match;
    }
}


























