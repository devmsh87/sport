<?php

use Faker\Generator as Faker;

$factory->define(App\League::class, function (Faker $faker) {
    return [
        'name' => $faker->country,
        'logo' => $faker->imageUrl(),
        'followers_count' => rand(10,10000),
        'country_id' => function(){
            return factory(\App\Country::class)->create()->id;
        },
        'association_id' => function(){
            return factory(\App\Association::class)->create()->id;
        }
    ];
});