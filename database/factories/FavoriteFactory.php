<?php

use Faker\Generator as Faker;

$factory->define(App\Favorite::class, function (Faker $faker) {
    return [
        'user_id' => function(){
            return factory(\App\User::class)->create();
        },
        'Favoritable_id' => function(){
            return factory(\App\League::class)->create();
        },
        'Favoritable_type' => \App\League::class,
    ];
});
