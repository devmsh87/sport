<?php

use Faker\Generator as Faker;

$factory->define(App\Match::class, function (Faker $faker) {
    return [
        'started_at' => \Carbon\Carbon::now()->addDays(1),
        'season_id' => function () {
            return factory(\App\Season::class)->create();
        },
        'stage_id' => function () {
            return factory(\App\Stage::class)->create();
        },
        'first_team_id' => function () {
            return factory(\App\Team::class)->create();
        },
        'second_team_id' => function () {
            return factory(\App\Team::class)->create();
        },
        'first_team_label' => $faker->randomLetter,
        'second_team_label' => $faker->randomLetter
    ];
});

$factory->state(App\Match::class, 'finished', function (Faker $faker) {
    return [
        'started_at' => \Carbon\Carbon::yesterday(),
    ];
});
