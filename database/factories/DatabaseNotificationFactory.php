<?php

use Faker\Generator as Faker;

$factory->define(\Illuminate\Notifications\DatabaseNotification::class, function (Faker $faker) {
    return [
        'data' => [
            'icon' => $faker->imageUrl(300,300),
            'title' => $faker->sentence(3),
            'description' => $faker->sentence(7),
        ]
    ];
});
