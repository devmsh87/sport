<?php

use Faker\Generator as Faker;

$factory->define(App\MatchTeam::class, function (Faker $faker) {
    return [
        'match_id' => function () {
            return factory(\App\Match::class)->create();
        },
        'first_team_id' => function () {
            return factory(\App\Team::class)->create();
        },
        'second_team_id' => function () {
            return factory(\App\Team::class)->create();
        },
        'first_team_label' => $faker->randomLetter,
        'second_team_label' => $faker->randomLetter
    ];
});
