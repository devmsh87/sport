<?php

use App\MatchResultPrediction;
use Faker\Generator as Faker;

$factory->define(App\MatchResultPrediction::class, function (Faker $faker) {
    return [
        'first_team_result' => $faker->numberBetween(1,5),
        'second_team_result' => $faker->numberBetween(1,5),
        'user_id' => function(){
            return factory(\App\User::class)->create();
        },
        'match_id' => function(){
            return factory(\App\Match::class)->create();
        }
    ];
});

$factory->state(App\MatchResultPrediction::class, 'lose', [
    'status' => MatchResultPrediction::STATUS_LOSE,
    'weight' => MatchResultPrediction::LOSE_WEIGHT,
]);
$factory->state(App\MatchResultPrediction::class, 'diff', [
    'status' => MatchResultPrediction::STATUS_DIFF,
    'weight' => MatchResultPrediction::DIFF_WEIGHT,
]);
$factory->state(App\MatchResultPrediction::class, 'exact', [
    'status' => MatchResultPrediction::STATUS_EXACT,
    'weight' => MatchResultPrediction::EXACT_WEIGHT,
]);
$factory->state(App\MatchResultPrediction::class, 'win', [
    'status' => MatchResultPrediction::STATUS_WIN,
    'weight' => MatchResultPrediction::WINNER_WEIGHT,
]);
