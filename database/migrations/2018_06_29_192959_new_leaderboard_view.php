<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewLeaderboardView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        return;

        try {
            DB::statement('DROP VIEW leaderboards');
        } catch (\Exception $exception) {
        }

        if (config("APP_ENV") != "testing") {
            DB::statement("CREATE VIEW leaderboards 
            AS 
            SELECT 
                mrp.*,
                m.season_id,
                s.league_id,
                mt.first_team_id,
                mt.second_team_id,
                m.stage_id
            from match_result_predictions mrp
            LEFT JOIN matches m ON m.id=mrp.match_id
            LEFT JOIN seasons s ON s.id=m.season_id
            LEFT JOIN match_teams mt ON m.id=mt.match_id
            WHERE mrp.status != 1
        ");
            return;
        }


        DB::statement("CREATE VIEW leaderboards AS
        (
            SELECT pn.*
            FROM `points` pn
            LEFT JOIN `predictions` pr ON pr.id= pn.reason_id
            LEFT JOIN `match_result_predictions` mrp ON mrp.id=pr.predictable_id
            LEFT JOIN `matches` m ON m.id=mrp.match_id
        )");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        return;
        // DB::statement('DROP VIEW leaderboards');
    }
}
