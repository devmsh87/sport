<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MergeMatchesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('matches', function (Blueprint $table) {
            $table->unsignedInteger('first_team_id')->nullable();
            $table->unsignedInteger('second_team_id')->nullable();

            $table->string('first_team_label')->nullable();
            $table->string('second_team_label')->nullable();

            $table->unsignedInteger('first_team_result')->nullable();
            $table->unsignedInteger('second_team_result')->nullable();

            $table->integer('position')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
