<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('shortcut');
            $table->string('logo');
            $table->unsignedInteger('country_id')->nullable();
            $table->unsignedInteger('followers_count')->default(0);
            $table->timestamps();
        });

        Schema::create('season_team', function (Blueprint $table) {
            $table->unsignedInteger('team_id');
            $table->unsignedInteger('season_id');
            $table->string('group')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
        Schema::dropIfExists('season_team');
    }
}
