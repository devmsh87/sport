<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_teams1', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('match_id');

            $table->unsignedInteger('first_team_id')->nullable();
            $table->unsignedInteger('second_team_id')->nullable();

            $table->string('first_team_label')->nullable();
            $table->string('second_team_label')->nullable();

            $table->unsignedInteger('first_team_result')->nullable();
            $table->unsignedInteger('second_team_result')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_teams');
    }
}
