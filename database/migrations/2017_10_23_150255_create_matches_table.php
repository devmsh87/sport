<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('season_id');
            $table->unsignedInteger('stage_id')->nullable();
            $table->unsignedInteger('winner_to_id')->nullable();
            $table->tinyInteger('winner_to_position')->nullable();
            $table->unsignedInteger('loser_to_id')->nullable();
            $table->tinyInteger('loser_to_position')->nullable();
            $table->timestamp('started_at');
            $table->integer('status')->default(0);
            $table->boolean('evaluated')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
