<?php

namespace Devmsh\ImageCropper;

use Intervention\Image\Facades\Image;
use Laravel\Nova\Fields\Field;
use Laravel\Nova\Http\Requests\NovaRequest;

class ImageCropper extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'image-cropper';

    /**
     * Hydrate the given attribute on the model based on the incoming request.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param  string $requestAttribute
     * @param  object $model
     * @param  string $attribute
     * @return void
     */
    protected function fillAttributeFromRequest(NovaRequest $request,
                                                $requestAttribute,
                                                $model,
                                                $attribute)
    {
        if ($request->exists($requestAttribute)) {
            $base64 = $request[$requestAttribute];

            if ($base64 == null) {
//                $model->{$attribute} = null;
            }

            if (starts_with($base64, 'data:image')) {
                $filename = $this->getRoundedLogo($base64);
                $model->{$attribute} = $filename;
            } else {
                $model->{$attribute} = $base64;
            }
        }
    }

    public function getRoundedLogo($value)
    {
        $img = Image::make($value)->resize(200, 200);

        $width = $img->getWidth();
        $height = $img->getHeight();

        $mask = Image::canvas($width, $height);

        // draw a white circle
        $mask->circle($width, $width / 2, $height / 2, function ($draw) {
            $draw->background('#fff');
        });

        $img->mask($mask, true);
        $img->resize(100, 100);

        $name = time();
        $file_name = "uploads/leagues/{$name}.png";
        $img->save(public_path("storage/" . $file_name));

        return $file_name;
    }
}
