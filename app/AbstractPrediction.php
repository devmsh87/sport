<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractPrediction extends Model {

    const STATUS_PENDING = 1;
    const STATUS_WIN = 2;
    const STATUS_LOSE = 3;

    protected $guarded = [];

    public function prediction() {
        return $this->morphOne(Prediction::class, 'predictable');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function scopeUnevaluated($query)
    {
        $query->where('status', self::STATUS_PENDING);
    }

    abstract static public function predict(array $data);
}
