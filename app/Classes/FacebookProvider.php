<?php

namespace App\Classes;

use App\User;
use GuzzleHttp\Client;

class FacebookProvider
{
    public function getUser($token)
    {
        $client = new Client();
        $res = $client->get("https://graph.facebook.com/me?fields=id,name,email&access_token=$token");
        $data = json_decode($res->getBody()->getContents());
        $data->access_token = $token;

        return $data;
    }

    public function getFriends(User $user)
    {
        $client = new Client();
        $res = $client->get("https://graph.facebook.com/v3.0/{$user->fb_id}/friends?access_token={$user->fb_auth}");
        $data = json_decode($res->getBody()->getContents(),true);

        return $data;
    }
}