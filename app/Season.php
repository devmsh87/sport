<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Spatie\Translatable\HasTranslations;

class Season extends Model
{
    use HasTranslations;

    public $translatable = ['name'];

    protected $guarded = [];

    public function league()
    {
        return $this->belongsTo(League::class);
    }

    public function matches()
    {
        return $this->hasMany(Match::class);
    }

    public function stages()
    {
        return $this->hasMany(Stage::class);
    }

    public function teams()
    {
        return $this->belongsToMany(Team::class)->withPivot('group');
    }

    public function toArray()
    {
        return array_merge(parent::toArray(), [
            'league_name' => $this->league->name,
        ]);
    }

    public function getLogoAttribute($value)
    {
        return $value ? url('storage/' . $value) : null;
    }

    public function setLogoAttribute($value)
    {
        if ($value == null) {
            $this->attributes["logo"] = null;
        }

        if (starts_with($value, 'data:image')) {
            $filename = $this->getRoundedLogo($value);
            $this->attributes["logo"] = $filename;
        } else {
            $this->attributes["logo"] = $value;
        }
    }

    public function getRoundedLogo($value)
    {
        $img = Image::make($value)->resize(200, 200);

        $width = $img->getWidth();
        $height = $img->getHeight();

        $mask = Image::canvas($width, $height);

        // draw a white circle
        $mask->circle($width, $width / 2, $height / 2, function ($draw) {
            $draw->background('#fff');
        });

        $img->mask($mask, true);
        $img->resize(100, 100);

        $file_name = "uploads/seasons/{$this->id}.png";
        $img->save(public_path($file_name));

        return $file_name;
    }
}
