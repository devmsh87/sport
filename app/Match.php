<?php

namespace App;

use App\Events\MatchFinished;
use App\Events\MatchIdsSet;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Laravel\Nova\Actions\Actionable;

class Match extends Model
{
    use Actionable;

    protected $with = ['firstTeam', 'secondTeam'];

    protected $guarded = [];

    protected $appends = [
        'time_status',
    ];

    protected $dates = [
        'started_at'
    ];

    public $predictionTypes = [
        'resultPrediction' => MatchResultPrediction::class,
        'teamPrediction' => MatchTeamPrediction::class,
    ];

    const POSITION_FIRST = 1;
    const POSITION_SECOND = 2;

    const STATUS_FINISHED = 1;
    const STATUS_EVALUATED = 2;
    const STATUS_LIVE = 3;
    const STATUS_SCHEDULED = 4;

    public function allowToFinish()
    {
        $this->started_at = Carbon::now()->subMinutes(106);
        return $this;
    }

    public static function markAsFinished()
    {
        return Carbon::now()->subMinutes(106);
    }

    public function resultPrediction()
    {
        return $this->hasMany(MatchResultPrediction::class);
    }

    public function teamPrediction()
    {
        return $this->hasMany(MatchTeamPrediction::class);
    }

    public function userTeamPrediction($user)
    {
        return $this->hasOne(MatchTeamPrediction::class)
            ->where('user_id', $user->id);
    }

    public function myTeamPrediction()
    {
        return $this->hasOne(MatchTeamPrediction::class)
            ->where('user_id', Auth::id());
    }

    public function usersTeamPrediction()
    {
        return $this->hasOne(MatchTeamPrediction::class);
    }

    public function myResultPrediction()
    {
        return $this->hasOne(MatchResultPrediction::class)
            ->where('user_id', Auth::id());
    }

    public function userResultPrediction()
    {
        return $this->hasOne(MatchResultPrediction::class);
    }

    public function results()
    {
        return $this->hasMany(UserMatchResult::class);
    }

    public function season()
    {
        return $this->belongsTo(Season::class);
    }

//    public function matchTeam()
//    {
//        return $this->hasOne(MatchTeam::class);
//    }

//    public function teams()
//    {
//        return $this->hasOne(MatchTeam::class);
//    }

    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    public function winnerTo()
    {
        return $this->belongsTo(Match::class);
    }

    public function loserTo()
    {
        return $this->belongsTo(Match::class);
    }

    public function addResult($first_result, $second_result)
    {
        if ($this->status != self::STATUS_FINISHED) {
            throw new \Exception("Cannot evaluate unfinished match");
        }

        $result = $this->results()->firstOrNew([
            'user_id' => Auth::id(),
            'match_id' => $this->id
        ]);

        $result->first_team_result = $first_result;
        $result->second_team_result = $second_result;
        $result->save();

        if (Auth::id() == 1 || Auth::id() == 1066 || Auth::id() == 7) {
            $this->finish($result->first_team_result, $result->second_team_result);
            event(new MatchFinished($this));
        } else {
            $this->checkResults();
        }

        return $result;
    }

    /**
     * @param $first_result
     * @param $second_result
     * @param null $position
     * @return MatchResultPrediction
     * @throws \Exception
     */
    public function predict($first_result, $second_result, $position = null)
    {
        if (Auth::guest()) {
            throw new \Exception("Guest cannot predict");
        } elseif (Auth::user()->points < MatchResultPrediction::USER_POINT_PER_PREDICTION) {
            throw new \Exception("User have no enough points");
        }
        if ($this->status == self::STATUS_LIVE) {
            throw new \Exception("Cannot predict match once it start");
        }
        if ($first_result == $second_result && $this->stage && !$this->stage->can_draw && !$position) {
            throw new \Exception("Cannot predict match end with draw");
        }

        if ($first_result > $second_result) {
            $position = self::POSITION_FIRST;
        }

        if ($first_result < $second_result) {
            $position = self::POSITION_SECOND;
        }

        return MatchResultPrediction::predict([
            'match_id' => $this->id,
            'first_result' => $first_result,
            'second_result' => $second_result,
            'position' => $position,
        ]);
    }

    public function finish($first_result, $second_result, $position = null)
    {
        if ($this->status != self::STATUS_FINISHED) {
            throw new \Exception("Cannot add result for unfinished match");
        }

        if ($first_result == $second_result && $this->stage && !$this->stage->can_draw && !$position) {
            throw new \Exception("Cannot end match with draw");
        }

        if ($first_result > $second_result) {
            $position = self::POSITION_FIRST;
        }

        if ($first_result < $second_result) {
            $position = self::POSITION_SECOND;
        }

        $this->first_team_result = $first_result;
        $this->second_team_result = $second_result;
        $this->position = $position;
        $this->save();
        $this->save();

        $this->moveWinner();
        $this->moveLoser();
    }

    public function moveWinner()
    {
        if ($this->winnerTo) {
            $matchTeams = $this->winnerTo;
            switch ($this->winner_to_position) {
                case self::POSITION_FIRST:
                    $matchTeams->first_team_id = $this->winner()->id;
                    break;
                case self::POSITION_SECOND:
                    $matchTeams->second_team_id = $this->winner()->id;
                    break;
            }
            $matchTeams->save();
        }
    }

    public function moveLoser()
    {
        if ($this->loserTo) {
            $matchTeams = $this->loserTo;
            switch ($this->loser_to_position) {
                case self::POSITION_FIRST:
                    $matchTeams->first_team_id = $this->loser()->id;
                    break;
                case self::POSITION_SECOND:
                    $matchTeams->second_team_id = $this->loser()->id;
                    break;
            }
            $matchTeams->save();
        }
    }

    public function winner()
    {
        switch ($this->winnerPosition()) {
            case self::POSITION_FIRST:
                return $this->firstTeam;
            case self::POSITION_SECOND:
                return $this->secondTeam;
            default:
                return null;
        }
    }

    public function loser()
    {
        switch ($this->loserPosition()) {
            case self::POSITION_FIRST:
                return $this->firstTeam;
            case self::POSITION_SECOND:
                return $this->secondTeam;
            default:
                return null;
        }
    }

    public function winnerPosition()
    {
        return $this->position;
    }

    public function loserPosition()
    {
        switch ($this->winnerPosition()) {
            case self::POSITION_FIRST:
                return self::POSITION_SECOND;
            case self::POSITION_SECOND:
                return self::POSITION_FIRST;
            default:
                return null;
        }
    }

    public function evaluate()
    {
        if ($this->status == self::STATUS_EVALUATED) {
            throw new \Exception("Match already evaluated");
        }

        if ($this->status != self::STATUS_FINISHED) {
            throw new \Exception("Cannot evaluate unfinished match");
        }

        MatchResultPrediction::evaluateMatch($this);

        $this->status = self::STATUS_EVALUATED;
        $this->evaluated = 1;
        $this->save();
    }

    public function getStatusAttribute()
    {
        if (isset($this->attributes['evaluated']) && $this->attributes['evaluated']) {
            return self::STATUS_EVALUATED;
        }

        $diff = Carbon::now()->diffInMinutes($this->started_at, false);
        $in_future = $diff <= 0;
        $diff = abs($diff);

        if ($in_future) {
            if ($diff > 105) {
                return self::STATUS_FINISHED;
            }

            return self::STATUS_LIVE;
        } else {
            return self::STATUS_SCHEDULED;
        }
    }

    public function getTimeStatusAttribute()
    {
        switch ($this->status) {
            case self::STATUS_EVALUATED:
                return "Evaluated";
                break;
            case self::STATUS_SCHEDULED:
                return "Scheduled";
                break;
            case self::STATUS_FINISHED:
                return "Finished";
                break;
            case self::STATUS_LIVE:
                return "Live";
                break;
        }
    }

    public function checkResults()
    {
        $recurrenceResult = $this->results()
            ->select(DB::RAW('count(id) as recurrence, first_team_result, second_team_result'))
            ->groupBy(['match_id', 'first_team_result', 'second_team_result'])
            ->having('recurrence', '>=', 3)
            ->first();

        if ($recurrenceResult) {
            $this->finish($recurrenceResult->first_team_result, $recurrenceResult->second_team_result);
            event(new MatchFinished($this));
        }
    }

    /**
     * @param $builder
     * @return mixed
     */
    public function getFriends($builder, $for = null)
    {
        $ids = $builder->pluck('user_id');
        $users = User::whereIn('id', $ids)->limit(50)->get(['id', 'points', 'name', 'fb_id']);
        return $users;
    }

//    /**
//     * @param $builder
//     * @return mixed
//     */
//    public function getFriends($builder, $for = null)
//    {
////        $user = Auth::user();
////        if (!$user) return [];
//
////        $friends = $user->friends()->pluck('friend_id');
////        $ids = $builder->whereIn('user_id', $friends)->pluck('user_id');
//        $ids = $builder->pluck('user_id');
//        $users = User::whereIn('id', $ids)->limit(50)->get(['id', 'points', 'name', 'fb_id']);
////        foreach ($users as $user) {
////            $mtp = MatchTeamPrediction::where('user_id', $user->id)->where('match_id', $this->id)->first();
////            if ($mtp) {
////                $team = null;
////                if ($for == self::POSITION_FIRST) $team = Team::find($mtp->first_team_id);
////                if ($for == self::POSITION_SECOND) $team = Team::find($mtp->second_team_id);
////                if ($team)
////                    $user->selected_team_flag = $team->logo;
////            }
////        }
////        $users = $users->sortBy('selected_team_flag')->values();
//        return $users;
//    }

    public function getStatsAttribute()
    {
        $count = $this->resultPrediction()->count();
        if (!$count) {
            return [
                'first_team_count' => 0,
                'second_team_count' => 0,
                'draw_count' => 0,
                'first_team_percentage' => 0,
                'second_team_percentage' => 0,
                'draw_percentage' => 0,
                'first_team_friends' => [],
                'second_team_friends' => [],
                'draw_friends' => [],
            ];
        }

        $firstTeamCount = $this->resultPrediction()->firstWin()->count();
        $secondTeamCount = $this->resultPrediction()->secondWin()->count();
        $drawCount = $this->resultPrediction()->draw()->count();

        return [
            'first_team_count' => $firstTeamCount,
            'second_team_count' => $secondTeamCount,
            'draw_count' => $drawCount,
            'first_team_friends' => $this->getFriends($this->resultPrediction()->firstWin(), self::POSITION_FIRST),
            'second_team_friends' => $this->getFriends($this->resultPrediction()->secondWin(), self::POSITION_SECOND),
            'draw_friends' => $this->getFriends($this->resultPrediction()->draw()),
            'first_team_percentage' => number_format($firstTeamCount / $count * 100, 2),
            'second_team_percentage' => number_format($secondTeamCount / $count * 100, 2),
            'draw_percentage' => number_format($drawCount / $count * 100, 2),
        ];
    }

    public function toArray()
    {
        $teamHolder = "https://cdn0.iconfinder.com/data/icons/sports-59/512/Soccer-512.png";

        $array = array_merge(parent::toArray(), [
            'season_name' => $this->season->name,
            'season_logo' => $this->season->logo,
            'start_time' => $this->started_at->format('H:i'),
            'start_date' => $this->started_at->format('d/m/Y'),
            'first_team_result' => $this->first_team_result,
            'second_team_result' => $this->second_team_result,
            'first_team_label' => $this->first_team_label,
            'second_team_label' => $this->second_team_label,
            'first_team_logo' => $teamHolder,
            'second_team_logo' => $teamHolder,
        ]);

        $array = array_merge($array, [
            'can_draw' => true,
        ]);

        if ($this->stage) {
            $array = array_merge($array, [
                'stage_name' => $this->stage ? $this->stage->name : null,
                'can_draw' => !!($this->stage ? $this->stage->can_draw : true),
            ]);
        }

        if ($this->secondTeam) {
            $array = array_merge($array, [
                'second_team_id' => $this->secondTeam->id,
                'second_team_name' => $this->secondTeam->name,
                'second_team_logo' => $this->secondTeam->logo,
            ]);
        } else {
            $firstMatchFrom = Match::where(function ($query) {
                $query->where('winner_to_id', $this->id)
                    ->where('winner_to_position', self::POSITION_FIRST);
            })->orWhere(function ($query) {
                $query->where('loser_to_id', $this->id)
                    ->where('loser_to_position', self::POSITION_FIRST);
            })->first();

            if ($firstMatchFrom) {
                $array = array_merge($array, [
                    'first_team_from_id' => $firstMatchFrom->id,
                ]);
            }
        }

        if ($this->firstTeam) {
            $array = array_merge($array, [
                'first_team_id' => $this->firstTeam->id,
                'first_team_name' => $this->firstTeam->name,
                'first_team_logo' => $this->firstTeam->logo,
            ]);
        } else {
            $secondMatchFrom = Match::where(function ($query) {
                $query->where('winner_to_id', $this->id)
                    ->where('winner_to_position', self::POSITION_SECOND);
            })->orWhere(function ($query) {
                $query->where('loser_to_id', $this->id)
                    ->where('loser_to_position', self::POSITION_SECOND);
            })->first();

            if ($secondMatchFrom) {
                $array = array_merge($array, [
                    'second_team_from_id' => $secondMatchFrom->id,
                ]);
            }
        }

        if ($this->relationLoaded('myTeamPrediction') && $this->myTeamPrediction) {
            if ($this->myTeamPrediction->firstTeam) {
                $array = array_merge($array, [
                    'predicted_first_team_id' => $this->myTeamPrediction->firstTeam->id,
                    'predicted_first_team_name' => $this->myTeamPrediction->firstTeam->name,
                    'predicted_first_team_logo' => $this->myTeamPrediction->firstTeam->logo,
                ]);
            }
            if ($this->myTeamPrediction->secondTeam) {
                $array = array_merge($array, [
                    'predicted_second_team_id' => $this->myTeamPrediction->secondTeam->id,
                    'predicted_second_team_name' => $this->myTeamPrediction->secondTeam->name,
                    'predicted_second_team_logo' => $this->myTeamPrediction->secondTeam->logo,
                ]);
            }
            $array = array_merge($array, [
                'team_prediction_status' => $this->myTeamPrediction->status,
            ]);

            unset($array['my_team_prediction']);
        }

        if ($this->relationLoaded('usersTeamPrediction') && $this->usersTeamPrediction) {
            if ($this->usersTeamPrediction->firstTeam) {
                $array = array_merge($array, [
                    'predicted_first_team_id' => $this->usersTeamPrediction->firstTeam->id,
                    'predicted_first_team_name' => $this->usersTeamPrediction->firstTeam->name,
                    'predicted_first_team_logo' => $this->usersTeamPrediction->firstTeam->logo,
                ]);
            }
            if ($this->usersTeamPrediction->secondTeam) {
                $array = array_merge($array, [
                    'predicted_second_team_id' => $this->usersTeamPrediction->secondTeam->id,
                    'predicted_second_team_name' => $this->usersTeamPrediction->secondTeam->name,
                    'predicted_second_team_logo' => $this->usersTeamPrediction->secondTeam->logo,
                ]);
            }
            $array = array_merge($array, [
                'team_prediction_status' => $this->usersTeamPrediction->status,
            ]);

            unset($array['users_team_prediction']);
        }

        if (isset($array['my_result_prediction'])) {
            $array['user_result_prediction'] = $array['my_result_prediction'];
            unset($array['my_result_prediction']);
        }

        return $array;
    }

    public function getFirstTeamNameAttribute()
    {
        $firstTeamName = $this->first_team_label;
        if ($this->firstTeam) {
            $firstTeamName = $this->firstTeam->name;
        }

        return $firstTeamName;
    }

    public function getSecondTeamNameAttribute()
    {
        $secondTeamName = $this->second_team_label;
        if ($this->secondTeam) {
            $secondTeamName = $this->secondTeam->name;
        }

        return $secondTeamName;
    }

    //////

    public function firstTeam()
    {
        return $this->belongsTo(Team::class);
    }

    public function secondTeam()
    {
        return $this->belongsTo(Team::class);
    }

    public function setTeamsIds($first_team_id, $second_team_id)
    {
        $this->update([
            "first_team_id" => $first_team_id,
            "second_team_id" => $second_team_id
        ]);

        event(new MatchIdsSet($this));
    }

    public function evaluateTeams()
    {
        MatchTeamPrediction::evaluateTeams($this);
    }

    public static function getUnSetTeamsNumber($stage_id)
    {
        return Match::where("stage_id", $stage_id)
            ->whereNull("first_team_id")
            ->whereNull("second_team_id")
            ->count();
    }
}
