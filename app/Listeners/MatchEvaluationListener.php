<?php

namespace App\Listeners;

use App\Events\MatchFinished;
use Illuminate\Contracts\Queue\ShouldQueue;

class MatchEvaluationListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MatchFinished $event
     * @return void
     */
    public function handle(MatchFinished $event)
    {
        $event->match->evaluate();
    }
}
