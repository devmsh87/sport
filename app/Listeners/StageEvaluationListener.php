<?php

namespace App\Listeners;

use App\Events\StageFinished;
use Illuminate\Contracts\Queue\ShouldQueue;

class StageEvaluationListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StageFinished $event
     * @return void
     */
    public function handle(StageFinished $event)
    {
        $event->stage->evaluate();
    }
}
