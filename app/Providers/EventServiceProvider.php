<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\MatchFinished' => [
            'App\Listeners\MatchEvaluationListener',
        ],
        'App\Events\MatchIdsSet' => [
            'App\Listeners\MatchTeamEvaluationListener',
        ],
        'App\Events\StageFinished' => [
            'App\Listeners\StageEvaluationListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
