<?php

namespace App\Repositories;

use App\Match;
use App\MatchResultPrediction;
use App\Season;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class MatchesRepository
{

    public static function getMatches($date, $filters)
    {
        $date = Carbon::createFromFormat("d-m-Y", $date);

        $teamId = $filters->team_id;
        $seasonId = $filters->season_id;
        $leagueId = $filters->league_id;
        $stageId = $filters->stage_id;
        $userId = $filters->user_id;

        if ($leagueId)
            $seasonId = Season::where('league_id', $leagueId)->firstOrFail()->id;

        $matches = new \App\Match();

        if ($teamId || $seasonId || $stageId) {
            $matches = $matches->limit(50)->orderBy('started_at', 'desc');
        } else {
            $matches = $matches->orderBy('started_at', 'asc');
        }

        if ($filters->get('isPredict') == "true") {
            $matchesIds = MatchResultPrediction::where('user_id', Auth::id())->pluck('match_id');
            $matches = Match::whereIn('id', $matchesIds)->with('myResultPrediction')->orderBy('started_at', 'desc');
        } elseif ($userId) {
            $matchesIds = MatchResultPrediction::where('user_id', $userId)->pluck('match_id');
            $matches = Match::whereIn('id', $matchesIds)->with(['userResultPrediction' => function ($query) use ($userId) {
                $query->where('user_id', $userId);
            }])->orderBy('started_at', 'desc');
        } elseif ($seasonId) {
            $matches = $matches->where('season_id', $seasonId);
        } elseif ($stageId) {
            $matches = $matches->where('stage_id', $stageId);
        } elseif ($teamId) {
            $matches = $matches->where('first_team_id', $teamId)
                ->orWhere('second_team_id', $teamId);
        } elseif ($date) {
            if (Carbon::now()->diffInDays($date) > 1) {
                $matches = $matches->whereDate('started_at', $date->toDateString());
            } else {
                $matches = $matches->whereDate('started_at', '>=', $date->toDateString());
            }
        }

        if ($userId) {
            $matches->with(['usersTeamPrediction' => function ($query) use ($userId) {
                $query->where('user_id', $userId);
            }]);
        } else {
            $matches->with('myTeamPrediction.firstTeam', 'myTeamPrediction.secondTeam');
        }

        $matches = collect($matches->get()->toArray());

        $data = $matches->groupBy('start_date')->map(function ($dateGroup) {
            return $dateGroup->groupBy('season_id');
        });

        $json = [];
        foreach ($data as $date => $seasonData) {
            $carbonDate = Carbon::createFromFormat('d/m/Y', $date);
            if (Carbon::yesterday()->diffInDays($carbonDate) == 0) {
                $date = "Yesterday";
            } elseif (Carbon::today()->diffInDays($carbonDate) == 0) {
                $date = "Today";
            } elseif (Carbon::tomorrow()->diffInDays($carbonDate) == 0) {
                $date = "Tomorrow";
            }

            $seasons = [];
            foreach ($seasonData as $matchesData) {
                $seasons [] = [
                    'id' => $matchesData[0]['season_id'],
                    'name' => $matchesData[0]['season_name'],
                    'round' => "Round 12",
                    'logo' => $matchesData[0]['season_logo'],
                    'matches' => $matchesData,
                ];
            }
            $json [] = [
                'date' => $date,
                'leagues' => $seasons,
            ];
        }

        if (empty($json)) {
            $json [] = [
                'date' => $date->format('d/m/Y'),
                'leagues' => [],
            ];
        }

        return $json;
    }

    public static function getBestMatches($date, $filters)
    {
        $date = Carbon::createFromFormat("d-m-Y", $date);

        $teamId = $filters->team_id;
        $seasonId = $filters->season_id;
        $leagueId = $filters->league_id;
        $stageId = $filters->stage_id;
        $userId = $filters->user_id;

        if ($leagueId)
            $seasonId = Season::where('league_id', $leagueId)->firstOrFail()->id;

        $matches = new \App\Match();

        if ($teamId || $seasonId || $stageId) {
            $matches = $matches->limit(50)->orderBy('started_at', 'desc');
        } else {
            $matches = $matches->orderBy('started_at', 'asc');
        }

        if ($filters->get('isPredict') == "true") {
            $matchesIds = MatchResultPrediction::where('user_id', Auth::id())->pluck('match_id');
            $matches = Match::whereIn('id', $matchesIds)->with('myResultPrediction')->orderBy('started_at', 'desc');
        } elseif ($userId) {
            $matchesIds = MatchResultPrediction::where('user_id', $userId)->pluck('match_id');
            $matches = Match::whereIn('id', $matchesIds)->with(['userResultPrediction' => function ($query) use ($userId) {
                $query->where('user_id', $userId);
            }])->orderBy('started_at', 'desc');
        } elseif ($seasonId) {
            $matches = $matches->where('season_id', $seasonId);
        } elseif ($stageId) {
            $matches = $matches->where('stage_id', $stageId);
        } elseif ($teamId) {
            $matches = $matches->where('first_team_id', $teamId)
                ->orWhere('second_team_id', $teamId);
        } elseif ($date) {
            if (Carbon::now()->diffInDays($date) > 1) {
                $matches = $matches->whereDate('started_at', $date->toDateString());
            } else {
                $matches = $matches->whereDate('started_at', '>=', $date->toDateString());
            }
        }

        if ($userId) {
            $matches->with(['usersTeamPrediction' => function ($query) use ($userId) {
                $query->where('user_id', $userId);
            }]);
        } else {
            $matches->with('myTeamPrediction.firstTeam', 'myTeamPrediction.secondTeam');
        }

        $matches = collect($matches->get()->toArray());

        $data = $matches->groupBy('start_date')->map(function ($dateGroup) {
            return $dateGroup->groupBy('season_id');
        });

        $json = [];
        foreach ($data as $date => $seasonData) {
            $carbonDate = Carbon::createFromFormat('d/m/Y', $date);
            if (Carbon::yesterday()->diffInDays($carbonDate) == 0) {
                $date = "Yesterday";
            } elseif (Carbon::today()->diffInDays($carbonDate) == 0) {
                $date = "Today";
            } elseif (Carbon::tomorrow()->diffInDays($carbonDate) == 0) {
                $date = "Tomorrow";
            }

            $seasons = [];
            foreach ($seasonData as $matchesData) {
                $seasons [] = [
                    'id' => $matchesData[0]['season_id'],
                    'name' => $matchesData[0]['season_name'],
                    'round' => "Round 12",
                    'logo' => $matchesData[0]['season_logo'],
                    'matches' => $matchesData,
                ];
            }
            $json [] = [
                'date' => $date,
                'leagues' => $seasons,
            ];
        }

        if (empty($json)) {
            $json [] = [
                'date' => $date->format('d/m/Y'),
                'leagues' => [],
            ];
        }

        return $json;
    }


    public static function getPredictions($date, $filters, $isPredict)
    {
        $date = Carbon::createFromFormat("d-m-Y", $date);

        if ($isPredict == "true") {
            $query = \App\MatchResultPrediction::where('user_id', Auth::id());
        } else {
            $query = \App\MatchResultPrediction::whereHas('match', function ($query) use ($date, $filters) {
                $teamId = $filters->team_id;
                $seasonId = $filters->season_id;
                $leagueId = $filters->league_id;
                $stageId = $filters->stage_id;
                $matchId = $filters->match_id;

                if ($leagueId)
                    $seasonId = Season::where('league_id', $leagueId)->firstOrFail()->id;

                if ($matchId) {
                    $query->where('id', $matchId);
                } elseif ($seasonId) {
                    $query->where('season_id', $seasonId);
                } elseif ($stageId) {
                    $query->where('stage_id', $stageId);
                } elseif ($teamId) {
                    $query->whereHas('teams', function ($query) use ($teamId) {
                        $query->where('first_team_id', $teamId)
                            ->orWhere('second_team_id', $teamId);
                    });
                } elseif ($date) {
                    $query->whereDate('started_at', $date->toDateString());
                }
            })->where('user_id', Auth::id());
        }

        return $query->get();
    }


}