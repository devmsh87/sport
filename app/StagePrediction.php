<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class StagePrediction extends AbstractPrediction
{
    protected $attributes = [
        'status' => self::STATUS_PENDING,
    ];

    protected $casts = [
        'json' => 'array'
    ];

    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    static public function predict(array $data)
    {
        /** @var Stage $stage */
        [$stage, $teams] = $data;
        $json = count($data) > 2 ? $data[2] : "{}";

        $stagePrediction = StagePrediction::updateOrCreate([
            'stage_id' => $stage->id,
            'user_id' => Auth::id(),
        ], [
            'finished_matches' => $stage->finishMatches()->count(),
            'json' => $json
        ]);

        foreach (Stage::STAGE_MAP[$stage->type] as $match) {
            MatchTeamPrediction::predict([$stagePrediction, $match, $stage, $teams]);
        }
    }

    static public function predictMatch($match, $firstTeam, $secondTeam)
    {
        $stagePrediction = StagePrediction::firstOrCreate([
            'stage_id' => $match->stage->id,
            'user_id' => Auth::id(),
        ], [
            'finished_matches' => 0,
            'json' => "{}"
        ]);

        MatchTeamPrediction::predictMatch($match, $stagePrediction, $firstTeam, $secondTeam);
    }

    static public function predictTeam($match, $team, $position)
    {
        if ($position == Match::POSITION_FIRST) {
            StagePrediction::predictMatch($match, $team, null);
        } else {
            StagePrediction::predictMatch($match, null, $team);
        }
    }
}
