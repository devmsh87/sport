<?php

namespace App\Rules;

use App\MatchResultPrediction;
use Illuminate\Contracts\Validation\Rule;

class HaveEnoughPoints implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return auth()->user()->points >= MatchResultPrediction::USER_POINT_PER_PREDICTION;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'No enough point for this prediction';
    }
}
