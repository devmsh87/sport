<?php

namespace App\Rules;

use App\Match;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class CanPredictStage implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return true;
        // TODO return it back
        return $value->scheduledMatches()->count();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You cannot predict on this stage';
    }
}
