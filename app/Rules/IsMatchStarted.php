<?php

namespace App\Rules;

use App\Match;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class IsMatchStarted implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $match)
    {
        if (!$match || $match->started_at < Carbon::now())
            return false;
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You can\'t make prediction after match start';
    }
}
