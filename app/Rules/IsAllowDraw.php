<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IsAllowDraw implements Rule
{
    protected $first_team_result;
    protected $second_team_result;
    protected $position;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($first_team_result, $second_team_result, $position)
    {
        $this->first_team_result = $first_team_result;
        $this->second_team_result = $second_team_result;
        $this->position = $position;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!$value->stage)
            return true;

        if ($value->stage->can_draw) {
            return true;
        } else {
            if ($this->first_team_result == $this->second_team_result) {
                if ($this->position)
                    return true;
                else
                    return false;
            }

            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This match cannot end in draw';
    }
}
