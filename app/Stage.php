<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Stage extends Model
{
    const STAGE_GROUPS = 32;
    const STAGE_16 = 16;
    const STAGE_8 = 8;
    const STAGE_4 = 4;
    const STAGE_THIRD = 2;
    const STAGE_FINAL = 1;

    const STATUS_SCHEDULED = 4;
    const STATUS_LIVE = 3;
    const STATUS_FINISHED = 1;
    const STATUS_EVALUATED = 2;

    const STAGE_16_MAP = [
        ["C1", "D2"],
        ["A1", "B2"],
        ["B1", "A2"],
        ["D1", "C2"],
        ["E1", "F2"],
        ["G1", "H2"],
        ["F1", "E2"],
        ["H1", "G2"],
    ];

    const STAGE_8_MAP = [
        ["W49", "W50"],
        ["W53", "W54"],
        ["W55", "W56"],
        ["W51", "W52"],
    ];

    const STAGE_MAP = [
        self::STAGE_GROUPS => self::STAGE_16_MAP,
        self::STAGE_16 => self::STAGE_16_MAP,
        self::STAGE_8 => self::STAGE_8_MAP
    ];

    protected $guarded = [];

    public function season()
    {
        return $this->belongsTo(Season::class);
    }

    public function stagePrediction()
    {
        return $this->hasMany(StagePrediction::class);
    }

    public function matches()
    {
        return $this->hasMany(Match::class);
    }

    public function match()
    {
        return $this->hasOne(Match::class);
    }

    public function evaluatedMatches()
    {
        return $this->hasMany(Match::class)->where('status', self::STATUS_EVALUATED);
    }

    public function scheduledMatches()
    {
        return $this->hasMany(Match::class)->whereDate('started_at', '>', Carbon::now());
    }

    public function finishMatches()
    {
        return $this->hasMany(Match::class)->whereDate('started_at', '<', Carbon::now());
    }

    public function getStatusAttribute()
    {
        if ($this->attributes['evaluated']) {
            return self::STATUS_EVALUATED;
        }

        $status = self::STATUS_SCHEDULED;

        $hasEvaluated = $this->evaluatedMatches()->count();
        $hasScheduled = $this->scheduledMatches()->count();
        $hasFinished = $this->finishMatches()->count();

        if ($hasFinished && $hasScheduled) {
            return self::STATUS_LIVE;
        }

        if ($hasFinished == $hasEvaluated && !$hasScheduled) {
            return self::STATUS_FINISHED;
        }

        return $status;
    }

    public function evaluate()
    {
        if ($this->status == self::STATUS_EVALUATED) {
            throw new \Exception("Stage already evaluated");
        }

        if ($this->status != self::STATUS_FINISHED) {
            throw new \Exception("Cannot evaluate unfinished stage");
        }

        // TODO then we can evaluate the StagePrediction to calculate the user weight
        $this->stagePrediction()->update([
            'weight' => $this->weight //* ("percent of right prediction") * ("percent of unfinished matches"),
        ]);

        $this->evaluated = true;
        $this->save();
    }

    public function getLogoAttribute()
    {
        $map = [
            self::STAGE_GROUPS => url('storage/uploads/group_stage.png'),
            self::STAGE_16 => url('storage/uploads/round_of_16.png'),
            self::STAGE_8 => url('storage/uploads/quarter_final.png'),
            self::STAGE_4 => url('storage/uploads/semi_final.png'),
            self::STAGE_THIRD => url('storage/uploads/third_place.png'),
            self::STAGE_FINAL => url('storage/uploads/finals.png'),
        ];

        return $map[$this->type];
    }
}
