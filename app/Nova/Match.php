<?php

namespace App\Nova;

use App\Nova\Filters\MatchStatus;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Status;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Panel;

class Match extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Match';

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
//        'firstTeam' => ['name'],
        'season' => ['name'],
        'secondTeam' => ['name'],
    ];

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            BelongsTo::make('Season')->searchable(),
            DateTime::make('Started at'),
            BelongsTo::make('First Team', 'firstTeam', Team::class)->searchable(),
            BelongsTo::make('Second Team', 'secondTeam', Team::class)->searchable(),
            BelongsTo::make('Stage')->hideFromIndex(),
            Boolean::make('Evaluated')->onlyOnIndex(),
            Select::make('Status')->options([
                '0' => 'Pending',
                '4' => 'Scheduled',
                '3' => 'Live',
                '1' => 'Finished',
                '2' => 'Evaluated',
            ])->displayUsingLabels()->onlyOnIndex(),
            Number::make('First team result')->hideFromIndex(),
            Number::make('Second team result')->hideFromIndex(),
            Text::make('First team label')->hideFromIndex(),
            Text::make('Second team label')->hideFromIndex(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new MatchStatus
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            new Actions\FinishMatch
        ];
    }
}
