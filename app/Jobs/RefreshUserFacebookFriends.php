<?php

namespace App\Jobs;

use App\Classes\FacebookProvider;
use App\Friend;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class RefreshUserFacebookFriends implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function handle(FacebookProvider $facebookProvider)
    {
        $friends = $facebookProvider->getFriends($this->user);
        foreach ($friends['data'] as $friend) {
            Friend::firstOrCreate([
                'user_id' => $this->user->id,
                'facebook_id' => $friend['id'],
                'friend_id' => User::where('fb_id',$friend['id'])->first()->id,
            ]);
        }
    }
}
