<?php

namespace App\Jobs;

use App\MatchResultPrediction;
use App\Notifications\FCMNotification;
use App\Notifications\MatchResultPredictionNotification;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;

class MatchEvaluationNotificationJob implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $match;
    public $firstTeamName;
    public $secondTeamName;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($match)
    {
        //
        $this->match = $match;

        $this->firstTeamName = $match->firstTeamName;
        $this->secondTeamName = $match->secondTeamName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->notifyExact();
        $this->notifyDiff();
        $this->notifyWin();
        $this->notifyLose();
    }

    private function notifyExact()
    {
        MatchResultPrediction::forMatch($this->match)->is(MatchResultPrediction::STATUS_EXACT)->chunk(10000, function ($items) {
            $pred = $items->first();
            $title = "Exact result prediction";
            $body = "You win {$pred->points} points from {$this->firstTeamName} & {$this->secondTeamName} match";
            $users = User::whereIn('id', $items->pluck('user_id'))->get();
            Notification::send($users, new MatchResultPredictionNotification($title,$body,$this->match->id));
            $tokens = User::whereIn('id', $items->pluck('user_id'))->pluck('device_token');
            FCMNotification::notify($tokens, $title, $body, ["match_id" => $this->match->id]);
        });
    }

    private function notifyDiff()
    {
        MatchResultPrediction::forMatch($this->match)->is(MatchResultPrediction::STATUS_DIFF)->chunk(10000, function ($items) {
            $pred = $items->first();
            $title = "Goal difference prediction";
            $body = "You win {$pred->points} points from {$this->firstTeamName} & {$this->secondTeamName} match";
            $users = User::whereIn('id', $items->pluck('user_id'))->get();
            Notification::send($users, new MatchResultPredictionNotification($title,$body,$this->match->id));
            $tokens = User::whereIn('id', $items->pluck('user_id'))->pluck('device_token');
            FCMNotification::notify($tokens, $title, $body, ["match_id" => $this->match->id]);
        });
    }

    private function notifyWin()
    {
        MatchResultPrediction::forMatch($this->match)->is(MatchResultPrediction::STATUS_WIN)->chunk(10000, function ($items) {
            $pred = $items->first();
            $title = "Winner prediction";
            $body = "You win {$pred->points} points from {$this->firstTeamName} & {$this->secondTeamName} match";
            $users = User::whereIn('id', $items->pluck('user_id'))->get();
            Notification::send($users, new MatchResultPredictionNotification($title,$body,$this->match->id));
            $tokens = User::whereIn('id', $items->pluck('user_id'))->pluck('device_token');
            FCMNotification::notify($tokens, $title, $body, ["match_id" => $this->match->id]);
        });
    }

    private function notifyLose()
    {
        MatchResultPrediction::forMatch($this->match)->is(MatchResultPrediction::STATUS_LOSE)->chunk(10000, function ($items) {
            $pred = $items->first();
            $title = "Wrong prediction";
            $body = "You lost 10 points from {$this->firstTeamName} & {$this->secondTeamName} match";
            $users = User::whereIn('id', $items->pluck('user_id'))->get();
            Notification::send($users, new MatchResultPredictionNotification($title,$body,$this->match->id));
            $tokens = User::whereIn('id', $items->pluck('user_id'))->pluck('device_token');
            FCMNotification::notify($tokens, $title, $body, ["match_id" => $this->match->id]);
        });
    }
}
