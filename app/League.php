<?php

namespace App;

use function GuzzleHttp\Psr7\parse_header;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Spatie\Translatable\HasTranslations;

class League extends Model
{
    use HasTranslations;

    public $translatable = ['name'];

    protected $guarded = [];

//    protected $with = ['country'];

//    protected $appends = ['country_name'];

    public function getCountryNameAttribute()
    {
        return $this->country->name;
    }

    public function association()
    {
        return $this->belongsTo(Association::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function teams()
    {
        return $this->belongsToMany(Team::class);
    }

    public function season()
    {
        return $this->hasOne(Season::class);
    }

    public function seasons()
    {
        return $this->hasMany(Season::class);
    }

    public function getLogoAttribute($value)
    {
        return $value ? url('storage/' . $value) : null;
    }

    public function setLogoAttribute($value)
    {
        if ($value == null) {
            $this->attributes["logo"] = null;
        }

        if (starts_with($value, 'data:image')) {
            $filename = $this->getRoundedLogo($value);
            $this->attributes["logo"] = $filename;
        } else {
            $this->attributes["logo"] = $value;
        }
    }

    public function getRoundedLogo($value)
    {
        $img = Image::make($value)->resize(200, 200);

        $width = $img->getWidth();
        $height = $img->getHeight();

        $mask = Image::canvas($width, $height);

        // draw a white circle
        $mask->circle($width, $width / 2, $height / 2, function ($draw) {
            $draw->background('#fff');
        });

        $img->mask($mask, true);
        $img->resize(100, 100);

        $file_name = "uploads/leagues/{$this->id}.png";
        $img->save(public_path($file_name));

        return $file_name;
    }
}
