<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class MatchTeamPrediction extends AbstractPrediction
{
    const STATUS_EXACT = 2;
    const STATUS_FIRST_TEAM_RIGHT = 21;
    const STATUS_SECOND_TEAM_RIGHT = 22;
    const STATUS_LOSE = 3;

    protected $attributes = [
        'status' => self::STATUS_PENDING,
    ];

    public function match()
    {
        return $this->belongsTo(Match::class);
    }

    public function firstTeam()
    {
        return $this->belongsTo(Team::class);
    }

    public function secondTeam()
    {
        return $this->belongsTo(Team::class);
    }

    static public function predict(array $data)
    {
        [$stagePrediction, $match, $stage, $teams] = $data;

        $matchTeam = Match::where('first_team_label', $match[0])
            ->where('second_team_label', $match[1])
            ->firstOrFail();

        return MatchTeamPrediction::updateOrCreate([
            'match_id' => $matchTeam->id,
            'stage_id' => $stage->id,
            'stage_prediction_id' => $stagePrediction->id,
            'user_id' => Auth::id(),
        ], [
            'first_team_id' => $teams[$match[0]],
            'second_team_id' => $teams[$match[1]],
        ]);
    }

    static public function predictMatch($match, $stagePrediction, $firstTeam, $secondTeam)
    {
        $teamData = [];
        if ($firstTeam) {
            $teamData['first_team_id'] = $firstTeam->id;
        }
        if ($secondTeam) {
            $teamData['second_team_id'] = $secondTeam->id;

        }
        $prediction = MatchTeamPrediction::updateOrCreate([
            'match_id' => $match->id,
            'stage_id' => $stagePrediction->stage->id,
            'stage_prediction_id' => $stagePrediction->id,
            'user_id' => Auth::id(),
        ], $teamData);

        $matchResultPrediction = MatchResultPrediction::where('match_id', $match->id)
            ->where('user_id', Auth::id())->first();
        if ($matchResultPrediction) {
            MatchResultPrediction::predict([
                'match_id' => $match->id,
                'first_result' => $matchResultPrediction->first_team_result,
                'second_result' => $matchResultPrediction->second_team_result,
                'position' => $matchResultPrediction->position,
            ]);
        }

        return $prediction;
    }

    public static function evaluateTeams(Match $match)
    {
        self::unevaluated()->where('match_id', $match->id)
            ->where('first_team_id', $match->first_team_id)
            ->where('second_team_id', $match->second_team_id)
            ->update([
                'status' => self::STATUS_EXACT,
            ]);

        self::unevaluated()->where('match_id', $match->id)
            ->where('first_team_id', $match->first_team_id)
            ->where('second_team_id', '!=', $match->second_team_id)
            ->update([
                'status' => self::STATUS_FIRST_TEAM_RIGHT,
            ]);

        self::unevaluated()->where('match_id', $match->id)
            ->where('first_team_id', '!=', $match->first_team_id)
            ->where('second_team_id', $match->second_team_id)
            ->update([
                'status' => self::STATUS_SECOND_TEAM_RIGHT,
            ]);

        self::unevaluated()->where('match_id', $match->id)
            ->where('first_team_id', '!=', $match->first_team_id)
            ->where('second_team_id', '!=', $match->second_team_id)
            ->update([
                'status' => self::STATUS_LOSE,
            ]);
    }

    public static function getPredictions($date, $filters, $user_id, $isPredict)
    {
        $date = Carbon::createFromFormat("d-m-Y", $date);

        $query = self::whereHas('match', function ($query) use ($date) {
            $query->whereDate('started_at', $date->toDateString());
        })->where('user_id', $user_id);

        return $query->get();
    }

    public function getTeamByPosition($position)
    {
        return $position == Match::POSITION_FIRST ? $this->firstTeam : $this->secondTeam;
    }
}
