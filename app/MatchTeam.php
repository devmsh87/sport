<?php

namespace App;

use App\Events\MatchIdsSet;
use Illuminate\Database\Eloquent\Model;

class MatchTeam extends Model
{
    protected $fillable = ["first_team_id", "second_team_id"];

    public function match()
    {
        return $this->belongsTo(Match::class);
    }
}
