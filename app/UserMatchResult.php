<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMatchResult extends Model {

    protected $guarded = [];
}
