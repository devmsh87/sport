<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model {

    const REGISTRATION = 1000;

    protected $guarded = [];

    public function reason() {
        return $this->morphTo();
    }
}
