<?php

namespace App\Http\Requests\API;

use App\Rules\CanPredictStage;

class PredictStageRequest extends SuperRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'stage' => ["required", new CanPredictStage()]
        ];
    }

    protected function validationData()
    {
        $this->merge($this->route()->parameters());
        return $this->all();
    }
}