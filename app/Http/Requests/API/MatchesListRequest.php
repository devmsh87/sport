<?php

namespace App\Http\Requests\API;

use Illuminate\Validation\Rule;

class MatchesListRequest extends SuperRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'nullable|date_format:d-m-Y',
            'team_id' => ['nullable', 'integer', Rule::exists('teams', 'id')],
            'season_id' => ['nullable', 'integer', Rule::exists('seasons', 'id')],
            'league_id' => ['nullable', 'integer', Rule::exists('leagues', 'id')]
        ];
    }
}