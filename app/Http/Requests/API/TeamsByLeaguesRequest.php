<?php

namespace App\Http\Requests\API;

use Illuminate\Validation\Rule;

class TeamsByLeaguesRequest extends SuperRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'league_id' => ['nullable', 'integer', Rule::exists("leagues", 'id')]
        ];
    }
}