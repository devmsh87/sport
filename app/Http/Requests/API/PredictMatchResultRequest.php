<?php

namespace App\Http\Requests\API;

use App\Rules\HaveEnoughPoints;
use App\Rules\IsAllowDraw;
use App\Rules\IsHaveLessThan;
use App\Rules\IsMatchStarted;

class PredictMatchResultRequest extends SuperRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'match' => [
                new IsMatchStarted(), new IsHaveLessThan(14),
                new HaveEnoughPoints(),
                new IsAllowDraw($this->first_team_result, $this->second_team_result,$this->position)
            ],
            'first_team_result' => ['required', "integer"],
            'second_team_result' => ['required', "integer"]
        ];
    }

    protected function validationData()
    {
        $this->merge($this->route()->parameters());
        return $this->all();
    }
}