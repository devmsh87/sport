<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\PredictMatchResultRequest;
use App\Match;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MatchResultPredictionController extends Controller
{
    public function store(PredictMatchResultRequest $request, Match $match)
    {
        return $match->predict($request->first_team_result, $request->second_team_result,$request->position);
    }
}
