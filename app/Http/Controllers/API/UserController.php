<?php

namespace App\Http\Controllers\API;

use App\Classes\FacebookProvider;
use App\Friend;
use App\Http\Controllers\Controller;
use App\Jobs\RefreshUserFacebookFriends;
use App\Leaderboard;
use App\League;
use App\Notifications\AppRegistrationNotification;
use App\Point;
use App\Season;
use App\Stage;
use App\Team;
use App\User;
use App\UserStory;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        if (array_key_exists('HTTP_AUTHORIZATION', $_SERVER)) {
            $this->middleware('auth:api');
        }
    }

    public function login(FacebookProvider $facebookProvider)
    {
        $token = request('access_token');
        $user = $facebookProvider->getUser($token);
        $user->device_token = request('device_token');

        $user = User::fromFacebookUser($user)->fresh();

        $user->token = $user->createToken('access_token')->accessToken;

//        RefreshUserFacebookFriends::dispatch($user);

        return $user;
    }

    public function me(Request $request)
    {
        $user = $request->user();
        $user->load('coaching', 'secondCoaching', 'thirdCoaching');
        if ($request->fcm) {
            $user->device_token = $request->fcm;
        }
        $user->save();
        return $user;
    }

    public function show(User $user, Request $request)
    {
        $user->load('matchResultPredictions.match');
        $user->load('coaching', 'secondCoaching', 'thirdCoaching');
        $user->is_followed = false;
        if (\Auth::check()) {
            $user->is_followed = \Auth::user()->friends()->where('friend_id', $user->id)->exists();
        }
        $user->points = Leaderboard::points($user);
        return $user;
    }

    public function complete(Request $request)
    {
        return $request->user()->complete();
    }

    public function guide()
    {
        $userStory1 = new UserStory();
        $userStory1->image = url("uploads/sliders/predict_match.png");
        $userStory1->title = "Predict match result";
        $userStory1->description = "you can gain points by predict the matches results";

        $userStory2 = new UserStory();
        $userStory2->image = url("uploads/sliders/predict_group.png");
        $userStory2->title = "Predict Stages";
        $userStory2->description = "You can also predict the whole stage, and get extra points from that";

        $userStory3 = new UserStory();
        $userStory3->image = url("uploads/sliders/stages.png");
        $userStory3->title = "Track your points";
        $userStory3->description = "Once a match or stage finished, you can track your stage or match points and multiplication";

        $userStory4 = new UserStory();
        $userStory4->image = url("uploads/sliders/leaderboard.png");
        $userStory4->title = "Get new records";
        $userStory4->description = "compete your friends with a good predictions and keep your name in the top of the leaderboard";

        return [
            $userStory1,
            $userStory2,
            $userStory3,
            $userStory4
        ];
    }

    public function notifications()
    {
        if (\Auth::user()->notifications()->count() == 0) {
            \Auth::user()->notify(new AppRegistrationNotification());
        }
        $notifications = \Auth::user()->notifications()->limit(25)->get();

        return $notifications->map(function ($notification) {
            foreach ($notification->data as $key => $value) {
                $notification->$key = $value;
            }
            unset($notification->data);
            $notification->id = time();
            $notification->icon = public_path("uploads/pred.png");
            $notification->ago = $notification->created_at->diffForHumans();
            $notification->is_read = !!$notification->read_at;
            return $notification;
        });
    }

    public function leaderboard(Request $request)
    {
        $type = $request->type;
        $id = $request->id;
        if ($type && $id) {
            if ($type == "team") $for = Team::find($id);
            else if ($type == "league") $for = Season::find($id);
            else if ($type == "stage") $for = Stage::find($id);

            return Leaderboard::top(100, $for);
        } else {

            return Leaderboard::top(100);
        }
    }

    public function follow(User $user)
    {
        $friend = Friend::firstOrCreate([
            'user_id' => \Auth::id(),
            'facebook_id' => $user->fb_id,
            'friend_id' => $user->id,
        ]);

        if ($friend->wasRecentlyCreated) {
            $action = true;
        } else {
            $friend->delete();
            $action = false;
        }

        return [
            'status' => true,
            'result' => [
                'action' => $action
            ]
        ];
    }
}
