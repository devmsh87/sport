<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\TeamsByLeaguesRequest;
use App\Team;
use Illuminate\Support\Facades\Cache;

class TeamController extends Controller
{

    public function index(TeamsByLeaguesRequest $request)
    {
        if ($league_id = $request->league_id) {
//            return Cache::remember("teams-$league_id", 1440,function () use ($league_id) {
                return \App\League::find($league_id)->season->teams()->orderBy('followers_count','desc')->get() ?? [];
//            });
        }

//        return Cache::remember('teams', 1440, function () {
            return \App\Team::orderBy('followers_count','desc')->get();
//        });
    }

    public function show(Team $team)
    {
//        return Cache::remember("team-{$team->id}", 1440, function () use ($team) {
            return $team;
//        });
    }
}
