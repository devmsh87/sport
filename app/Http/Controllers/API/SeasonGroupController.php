<?php

namespace App\Http\Controllers\API;

use App\Season;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SeasonGroupController extends Controller
{
    public function index(Season $season)
    {
        $season->load('teams');

        return $season->teams->groupBy('pivot.group')->map(function($teams,$key){
            return [
                "name" => $key,
                "teams" => $teams
            ];
        })->values();
    }
}
