<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\AddMatchResultRequest;
use App\Match;
use App\Http\Controllers\Controller;

class MatchResultController extends Controller
{
    public function store(AddMatchResultRequest $request,Match $match)
    {
        return $match->addResult($request->first_team_result, $request->second_team_result);
    }
}
