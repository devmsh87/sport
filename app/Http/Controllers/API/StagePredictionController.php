<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\PredictStageRequest;
use App\Match;
use App\MatchTeam;
use App\MatchTeamPrediction;
use App\Season;
use App\Stage;
use App\StagePrediction;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class StagePredictionController extends Controller
{
    public function __construct()
    {
        if (array_key_exists('HTTP_AUTHORIZATION', $_SERVER)) {
            $this->middleware('auth:api');
        }
    }

    public function index()
    {
        if(\Auth::check()){
            $groups = StagePrediction::where('user_id',\Auth::id())->first();
            if($groups){
                return $groups->json;
            }
        }

        $season = Season::findOrFail(1);
        $season->load('teams');
        return $season->teams->groupBy('pivot.group')->map(function($teams,$key){
            return [
                "name" => $key,
                "teams" => $teams
            ];
        })->values();
    }

    public function store(PredictStageRequest $request, Stage $stage)
    {
        abort_if(!Auth::check(),401);

        if(!is_array($request->groups)){
            $request->groups = json_decode($request->groups,true);
        }

        $teams = [];
        foreach ($request->groups as $group) {
            $teams[$group['name']."1"] = $group['teams'][0]['id'];
            $teams[$group['name']."2"] = $group['teams'][1]['id'];
        }

        StagePrediction::predict([$stage, $teams,$request->groups]);

        return [
            'status' => true,
            'result' => [
                'action' => true,
            ]
        ];
    }
}
