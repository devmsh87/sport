<?php
/**
 * Created by PhpStorm.
 * User: devmsh
 * Date: 5/23/18
 * Time: 9:40 PM
 */

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController as BaseCrudController;

class CrudController extends BaseCrudController
{

    public function prepareFields()
    {
        $fields = collect($this->crud->getFields('both'));

        if($fields->has('logo')){
            $this->crud->addField([
                'name' => "logo",
                'type' => 'image',
                'upload' => true,
                'crop' => true,
                'aspect_ratio' => 1,
            ]);

            $this->crud->addColumn([
                'name' => 'logo',
                'type' => 'image',
            ]);
        }

    }

    public function addRelation($relation,$attribute = "name")
    {
        $this->crud->addField([
            'type' => 'select2',
            'name' => "{$relation}_id",
            'entity' => camel_case($relation),
            'attribute' => $attribute,
        ]);


        $this->crud->addColumn([
            'type' => "select",
            'name' => "{$relation}_id",
            'entity' => camel_case($relation),
            'attribute' => $attribute,
        ]);
    }
}