<?php
/**
 * Created by PhpStorm.
 * User: Unit
 * Date: 2/17/2018
 * Time: 12:51 AM
 */

namespace App\Notifications;

use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Message\Topics;

class FCMNotification
{

    /**
     * @param $tokens
     * @param $title
     * @param $body
     * @param null $data
     * @return mixed
     * @throws \Exception
     * @throws \LaravelFCM\Message\InvalidOptionException
     */
    public static function notify($tokens, $title, $body, $data = null)
    {
        return $tokens->chunk(1000)->map(function($tokens) use ($title, $body, $data) {
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 20);

            $notificationBuilder = new PayloadNotificationBuilder($title);
            $notificationBuilder->setBody($body)
                ->setSound('default');

            $dataBuilder = new PayloadDataBuilder();
            if ($data) {
                $dataBuilder->addData($data);
            }

            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();

            if (!$title) {
                $notification = null;
            }

            return FCM::sendTo($tokens->toArray(), $option, $notification, $data);
        });
    }

    /**
     * @param $tokens
     * @param $title
     * @param $body
     * @param null $data
     * @return mixed
     * @throws \Exception
     * @throws \LaravelFCM\Message\InvalidOptionException
     */
    public static function simpleNotify($tokens, $title, $body, $data = null)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        if ($data) {
            $dataBuilder->addData($data);
        }

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        if (!$title) {
            $notification = null;
        }

        return FCM::sendTo($tokens->toArray(), $option, $notification, $data);
    }

    public static function notifyTopic($topicName, $title, $body, $data)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        if ($data) {
            $dataBuilder->addData($data);
        }

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $topic = new Topics();
        $topic->topic($topicName);

        return FCM::sendToTopic($topic, $option, $notification, $data);
    }

    public static function notifyGroup($group, $title, $body, $data)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        if ($data) {
            $dataBuilder->addData($data);
        }

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        return FCM::sendToGroup($group, $option, $notification, $data);
    }
}