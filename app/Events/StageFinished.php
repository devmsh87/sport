<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class StageFinished
{
    use SerializesModels;
    public $stage;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($stage)
    {
        $this->stage = $stage;
    }
}