<?php

namespace App;

use App\Jobs\RefreshUserFacebookFriends;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{

    use Notifiable, HasApiTokens;

    protected $fillable = [
        'name', 'email', 'password', 'fb_id', 'points'
    ];

    protected $hidden = [
        'password', 'remember_token', 'fb_id', 'fb_auth', 'device_token', 'email'
    ];

    protected $casts = [
        'points' => 'double',
        'is_completed' => 'boolean'
    ];

    protected $appends = [
        'photo', 'rank', 'predictions_count', 'favorites_count'
    ];

    public function getPhotoAttribute()
    {
        return "https://graph.facebook.com/{$this->fb_id}/picture";
    }

    public function getRankAttribute()
    {
//        return Leaderboard::score($this);
        return User::where('points', '>', $this->points)->where('points', '!=', 1000)->count() + 1;
    }

    public function getPredictionsCountAttribute()
    {
        return $this->predictions()->count();
    }

    public function coaching()
    {
        return $this->hasMany(Team::class, 'coach_id');
    }

    public function secondCoaching()
    {
        return $this->hasMany(Team::class, 'second_coach_id');
    }

    public function thirdCoaching()
    {
        return $this->hasMany(Team::class, 'third_coach_id');
    }

    public function matchResultPredictions()
    {
        return $this->hasMany(\App\Models\MatchResultPrediction::class);
    }

    public function getFavoritesCountAttribute()
    {
        return $this->favorites()->count();
    }

    public function pointsHistory()
    {
        return $this->hasMany(Point::class);
    }

    public function routeNotificationForGcm()
    {
//        return "dpyck6DEEXw:APA91bHS-qyU8FRn_Q3MMayFNRXjMPfnbC63ue1SP4UK0Hkg-OTb8UCcN16wE1Hy_P-an-gS6yDi6OGp19ktP9ihr5Tf23kFwWbJwY_9fjq_GaKX1Tg6eYw-stB2-5FvhnH_9MiKUOQa";
//        return "dpyck6DEEXw:APA91bHS-qyU8FRn_Q3MMayFNRXjMPfnbC63ue1SP4UK0Hkg-OTb8UCcN16wE1Hy_P-an-gS6yDi6OGp19ktP9ihr5Tf23kFwWbJwY_9fjq_GaKX1Tg6eYw-stB2-5FvhnH_9MiKUOQa";
    }

    public function addPoints($points, $message, $reason = null)
    {
        $pointHistory = new Point([
            'points' => $points,
            'message' => $message
        ]);

        if ($reason) {
            $pointHistory->reason()->associate($reason);
        }

        $this->pointsHistory()->save($pointHistory);

        $this->points = $this->pointsHistory()->sum('points');
        $this->save();
    }

    public function subPoints($points, $message, $reason = null)
    {
        $this->addPoints(-$points, $message, $reason);
    }

    public static function fromFacebookUser($data)
    {
        $user = self::firstOrNew([
            'fb_id' => $data->id
        ]);

        if (!$user->exists) {
            $user->name = $data->name;
            $user->email = $data->email;
            $user->password = "password";
            $user->fb_auth = $data->access_token;
            $user->save();

            $user->addPoints(Point::REGISTRATION, "Registration points");
        }

        $user->device_token = $data->device_token;
        $user->save();

//        RefreshUserFacebookFriends::dispatch($user);

        return $user;
    }

    public function predictions()
    {
        return $this->hasMany(Prediction::class);
    }

    public function favorites()
    {
        return $this->hasMany(Favorite::class);
    }

    public function friends()
    {
        return $this->hasMany(Friend::class);
    }

    public function complete()
    {
        $this->is_completed = true;
        $this->save();

        return $this;
    }

}
