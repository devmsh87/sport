<?php

namespace App;

use App\Jobs\MatchEvaluationNotificationJob;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Queue;

class MatchResultPrediction extends AbstractPrediction
{

    const USER_POINT_PER_PREDICTION = 10;
    const TEAM_PREDICTION_VALUE_BOTH_RIGHT = 2;

    const TEAM_PREDICTION_VALUE_ONE_RIGHT = 1.5;
    const SYSTEM_POINT_PER_PREDICTION = 2;

    const STATUS_WIN = 2;
    const STATUS_LOSE = 3;
    const STATUS_EXACT = 21;
    const STATUS_DIFF = 22;

    const WINNER_WEIGHT = 1;
    const DIFF_WEIGHT = 1.5;
    const EXACT_WEIGHT = 2;
    const LOSE_WEIGHT = 0;

    protected $with = ['match'];

    protected $attributes = [
        'status' => self::STATUS_PENDING,
    ];

    protected static function boot()
    {
        parent::boot();

        self::saved(function (MatchResultPrediction $mrp) {
            $user = User::find($mrp->user_id);

            $prediction = \App\Prediction::firstOrCreate([
                'user_id' => $user->id,
                'predictable_type' => \App\MatchResultPrediction::class,
                'predictable_id' => $mrp->id,
            ]);

            if ($prediction->wasRecentlyCreated) {
                $user->subPoints(self::USER_POINT_PER_PREDICTION, "Prediction on match", $mrp);
            }

            $mrp->syncTeamPredictions();
        });
    }

    /**
     * @param Match $match
     * @return float|int
     */
    public static function getPointsPerWeights(Match $match)
    {
        $points = self::forMatch($match)->count() * (self::USER_POINT_PER_PREDICTION + self::SYSTEM_POINT_PER_PREDICTION);

        $weights = self::forMatch($match)->sum('weight');

        return $weights ? $points / $weights : 0;
    }

    public function match()
    {
        return $this->belongsTo(Match::class);
    }

    public function firstTeam()
    {
        return $this->belongsTo(Team::class);
    }

    public function secondTeam()
    {
        return $this->belongsTo(Team::class);
    }

    public static function evaluateMatch(Match $match)
    {
        self::evaluateWeights($match);

        self::evaluatePoints($match);

        self::updateUserPoints($match);

        MatchEvaluationNotificationJob::dispatch($match);
    }

    public static function evaluateWeights(Match $match)
    {
        self::forMatch($match)->unevaluated()->exactResult($match)->update([
            'weight' => self::EXACT_WEIGHT,
            'status' => self::STATUS_EXACT,
        ]);

        self::forMatch($match)->unevaluated()->rightDiff($match)->update([
            'weight' => self::DIFF_WEIGHT,
            'status' => self::STATUS_DIFF,
        ]);

        self::forMatch($match)->unevaluated()->rightWinner($match)->update([
            'weight' => self::WINNER_WEIGHT,
            'status' => self::STATUS_WIN,
        ]);

        self::forMatch($match)->unevaluated()->wrongWinner($match)->update([
            'weight' => self::LOSE_WEIGHT,
            'status' => self::STATUS_LOSE
        ]);
    }

    public static function evaluatePoints(Match $match)
    {
        $pointsPerWeights = self::getPointsPerWeights($match);

        self::forMatch($match)->wherePoints(0)->update(['points' => DB::RAW("`weight` * $pointsPerWeights")]);
        $minFunc = config('database.default') == 'sqlite_testing' ? "MIN" : "LEAST";
        self::forMatch($match)->wherePoints(0)->update(['points' => DB::RAW($minFunc . '(100,`points`)')]);

        if ($match->stage && !in_array($match->stage->id, [Stage::STAGE_GROUPS])) {
            $user_ids = $match->teamPrediction()->whereStatus(MatchTeamPrediction::STATUS_EXACT)->pluck('user_id');
            self::forMatch($match)->whereIn("user_id", $user_ids)->update([
                'points' => DB::RAW("`points` * 2"),
            ]);

            if ($match->winnerPosition() == Match::POSITION_FIRST) {
                $user_ids = $match->teamPrediction()->whereStatus(MatchTeamPrediction::STATUS_FIRST_TEAM_RIGHT)->pluck('user_id');
                self::forMatch($match)->whereIn("user_id", $user_ids)->update([
                    'points' => DB::RAW("`points` * 1.5"),
                ]);
            } elseif ($match->winnerPosition() == Match::POSITION_SECOND) {
                $user_ids = $match->teamPrediction()->whereStatus(MatchTeamPrediction::STATUS_SECOND_TEAM_RIGHT)->pluck('user_id');
                self::forMatch($match)->whereIn("user_id", $user_ids)->update([
                    'points' => DB::RAW("`points` * 1.5"),
                ]);
            }
        }

        self::forMatch($match)->wherePoints(0)->update([
            'points' => -10,
            'status' => MatchResultPrediction::STATUS_LOSE,
        ]);
    }

    /**
     * Copy the prediction results to the point table
     * and update the user point field
     * @param $match
     */
    public static function updateUserPoints($match)
    {
        $message = "You get new points from prediction";
        $reason_type = self::class;

        $winPrediction = $match->resultPrediction()->where('points', '>', 0)->selectRAW(
            "user_id, points , '$message' as message, '$reason_type' as reason_type, id as reason_id"
        );

        DB::insert("insert into points (user_id,points,message,reason_type,reason_id) " . $winPrediction->toSql(),
            $winPrediction->getBindings()
        );

        $userIds = $match->resultPrediction()->pluck('user_id')->implode(',');
        DB::update("update users set points = (select sum(points) from points where user_id = users.id) where id in ($userIds)");
    }

    /**
     * Create new prediction or update the current one
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public static function predict(array $data)
    {
        return \App\MatchResultPrediction::updateOrCreate([
            'user_id' => Auth::id(),
            'match_id' => $data['match_id'],
        ], [
            'first_team_result' => $data['first_result'],
            'second_team_result' => $data['second_result'],
            'position' => $data['position'],
            'points' => 0,
        ]);
    }

    public function winnerPosition()
    {
        return $this->position;
    }

    public function loserPosition()
    {
        switch ($this->winnerPosition()) {
            case Match::POSITION_FIRST:
                return Match::POSITION_SECOND;
            case Match::POSITION_SECOND:
                return Match::POSITION_FIRST;
            default:
                return null;
        }
    }

    public function winner()
    {
        switch ($this->winnerPosition()) {
            case Match::POSITION_FIRST:
                return $this->match->firstTeam;
            case Match::POSITION_SECOND:
                return $this->match->secondTeam;
            default:
                return null;
        }
    }

    public function loser()
    {
        switch ($this->loserPosition()) {
            case Match::POSITION_FIRST:
                return $this->match->firstTeam;
            case Match::POSITION_SECOND:
                return $this->match->secondTeam;
            default:
                return null;
        }
    }

    public function scopeIs($query, $status)
    {
        $query->where('status', $status);
    }

    public function scopeForMatch($query, $match)
    {
        $query->where('match_id', $match->id);
    }

    public function scopeExactResult($query, $result)
    {
        $query->where('first_team_result', $result->first_team_result)
            ->where('second_team_result', $result->second_team_result);
    }

    public function scopeRightDiff($query, $result)
    {
        $query->where(
            DB::raw('first_team_result - second_team_result'),
            $result->first_team_result - $result->second_team_result
        );
    }

    public function scopeRightWinner($query, Match $match)
    {
        $query->where("position", $match->winnerPosition());
    }

    public function scopeWrongWinner($query, Match $match)
    {
        $query->where("position", $match->loserPosition());
    }

    public function scopeFirstWin($query)
    {
        return $query->where("position", Match::POSITION_FIRST);
    }

    public function scopeSecondWin($query)
    {
        return $query->where("position", Match::POSITION_SECOND);
    }

    public function scopeDraw($query)
    {
        return $query->whereNull('position');
    }

    /**
     * Get the real winner, else return the predicted winner
     * @return Team
     */
    public function getWinner()
    {
        $winner = $this->match->winner();
        if (!$winner) {
            $winner = $this->winner();
        }
        if (!$winner) {
            /** @var MatchTeamPrediction $teamPrediction */
            if ($teamPrediction = $this->match->myTeamPrediction) {
                $winner = $teamPrediction->getTeamByPosition($this->winnerPosition());
            }
        }

        return $winner;
    }

    /**
     * Get the real loser, else return the predicted loser
     * @return Team
     */
    public function getLoser()
    {
        $loser = $this->match->loser();
        if (!$loser) {
            $loser = $this->loser();
        }
        if (!$loser) {
            /** @var MatchTeamPrediction $teamPrediction */
            if ($teamPrediction = $this->match->myTeamPrediction) {
                $loser = $teamPrediction->getTeamByPosition($this->loserPosition());
            }
        }

        return $loser;
    }

    public function syncTeamPredictions()
    {
        $this->load('match.loserTo', 'match.winnerTo');

        if ($match = $this->match->winnerTo) {
            StagePrediction::predictTeam($match, $this->getWinner(), $this->match->winner_to_position);
        }

        if ($match = $this->match->loserTo) {
            StagePrediction::predictTeam($match, $this->getLoser(), $this->match->loser_to_position);
        }
    }

    public function toArray()
    {
        return array_merge(parent::toArray(), [
            'match_date' => $this->match->started_at->format('d/m/Y'),
        ]);
    }
}
