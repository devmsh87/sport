<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Match;
use App\Notifications\FCMNotification;
use App\Team;
use Intervention\Image\Facades\Image;
use LaravelFCM\Response\DownstreamResponse;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/privacy', function () {
    return view('privacy');
});

Route::get('/ranking', function () {
    return \App\Leaderboard::ranking(Auth::user());
});

Route::get('/move', function () {
    \App\MatchTeam::chunk(100, function ($teams) {
        foreach ($teams as $team) {
            $match = Match::find($team->match_id);
            $match->first_team_id = $team->first_team_id;
            $match->second_team_id = $team->second_team_id;
            $match->first_team_label = $team->first_team_label;
            $match->second_team_label = $team->second_team_label;
            $match->first_team_result = $team->first_team_result;
            $match->position = $team->position;
            $match->save();
        }
    });

});

Route::get('/coaches', function () {
    foreach (Team::all() as $team) {
        $leaders = \App\Leaderboard::top(3, $team);
        $team->third_coach_id = $leaders->pop()['id'];
        $team->second_coach_id = $leaders->pop()['id'];
        $team->coach_id = $leaders->pop()['id'];
        $team->save();
    }
});


Route::get('/datafix', function () {
//    \App\MatchResultPrediction::all()->each(function($p){
//        $first_result = $p->first_team_result;
//        $second_result = $p->second_team_result;
//
//        if ($first_result > $second_result) {
//            $p->position = Match::POSITION_FIRST;
//        }elseif ($first_result < $second_result) {
//            $p->position = Match::POSITION_SECOND;
//        }else{
//            $p->position = $p->winner_id;
//        }
//
//        $p->save();
//    });

//    \App\MatchTeam::all()->each(function($p){
//        $first_result = $p->first_team_result;
//        $second_result = $p->second_team_result;
//
//        if ($first_result > $second_result) {
//            $p->position = Match::POSITION_FIRST;
//        }elseif ($first_result < $second_result) {
//            $p->position = Match::POSITION_SECOND;
//        }else{
//            $p->position = $p->winner_id;
//        }
//
//        $p->save();
//    });
});


Route::get('/staaa1', function () {
    $results = [];

    foreach (\App\User::all() as $user) {
        $total = \App\StagePrediction::where('user_id', $user->id)->sum('weight');
        \App\StagePrediction::updateOrCreate([
            'user_id' => $user->id,
            'stage_id' => 6,
        ], [
            'weight' => $total,
            'json' => [],
            'finished_matches' => 0,
        ]);
    }
    return;
    foreach (\App\User::all() as $user) {
        $stage_id = 4;
        $matchesIds = \App\Match::where('stage_id', $stage_id)->pluck('id');
        $winning = \App\MatchResultPrediction::whereIn('match_id', $matchesIds)->where('user_id', $user->id)->where('status', \App\MatchResultPrediction::STATUS_WIN)->count();
        $exact = \App\MatchResultPrediction::whereIn('match_id', $matchesIds)->where('user_id', $user->id)->where('status', \App\MatchResultPrediction::STATUS_EXACT)->count();
        $diff = \App\MatchResultPrediction::whereIn('match_id', $matchesIds)->where('user_id', $user->id)->where('status', \App\MatchResultPrediction::STATUS_DIFF)->count();

        $exact1 = 500 / count($matchesIds) * 1.2;
        $diff1 = $exact1 / 2;
        $winning1 = $diff1 / 2;
        $total = $exact * $exact1 + $diff * $diff1 + $winning * $winning1;

        $total = (int)min($total, 500);
        if ($total) {
            $p = \App\StagePrediction::firstOrCreate([
                'user_id' => $user->id,
                'stage_id' => $stage_id,
            ], [
                'json' => [],
                'finished_matches' => 0,
            ]);
            $p->weight = $total;
            $p->save();
        }

        if ($total) $results[] = [
            'name' => $user->name,
            'points' => $total,
            'exact' => $exact,
            'diff' => $diff,
            'winning' => $winning
        ];
    }
    $results = array_sort($results, 'points');
    var_dump($results);
});

Route::get('/asdasdasd', function () {
    set_time_limit(-1);
    foreach (\App\MatchResultPrediction::all() as $matchResultPrediction) {
        Auth::loginUsingId($matchResultPrediction->user_id);
        \App\MatchResultPrediction::predict([
            'match_id' => $matchResultPrediction->match_id,
            'first_result' => $matchResultPrediction->first_team_result,
            'second_result' => $matchResultPrediction->second_team_result,
            'position' => $matchResultPrediction->position,
        ]);
    }
});

Route::get('/evaluteTeams', function () {
    $stage = \App\Stage::find(1);
    $stage->evaluate();

    return;
    $matches = \App\MatchTeam::whereNotNull('first_team_id')
        ->whereNotNull('second_team_id')
        ->get();

    foreach ($matches as $matchTeam) {
        $match = $matchTeam->match;
        if ($match->stage && $match->stage->id == 2) {
            \App\MatchTeamPrediction::evaluateTeams($matchTeam);
        }
//        $firstTeam = $prediction->match->teams->firstTeam;
//        $secondTeam = $prediction->match->teams->secondTeam;
//        if ($firstTeam && $secondTeam) {
//            $rightFirst = $prediction->first_team_id == $firstTeam->id;
//            $rightSecond = $prediction->second_team_id == $secondTeam->id;
//            if($rightFirst && $secondTeam){
//                var_dump('GOOD JOB');
//            }elseif ($rightFirst){
//                var_dump('FIRST');
//            }elseif ($rightSecond){
//                var_dump('SECOND');
//            }
//        }
    }
});

Route::get('/fixPred', function () {
    foreach (\App\Match::where('evaluated', 1)->get() as $match) {
        \App\MatchResultPrediction::evaluateWeights($match);
        \App\MatchResultPrediction::evaluatePoints($match);
    }
});

Route::get('/fixNotification', function () {
    $notifications = \Illuminate\Notifications\DatabaseNotification::where('type', \App\Notifications\MatchResultPredictionNotification::class)
        ->get();

    foreach ($notifications as $notification) {
        $match = \App\Match::find($notification->data['match_id']);
        $firstTeamName = $match->teams->firstTeam->name;
        $secondTeamName = $match->teams->secondTeam->name;

        $pred = \App\MatchResultPrediction::where('match_id', $notification->data['match_id'])
            ->where('user_id', $notification->notifiable_id)
            ->first();
        if ($pred->status == 21) {
            $title = "Exact result prediction";
            $body = "You win {$pred->points} points from {$firstTeamName} & {$secondTeamName} match";
            $data['title'] = $title;
            $data['description'] = $body;
        } elseif ($pred->status == 22) {
            $title = "Goal difference prediction";
            $body = "You win {$pred->points} points from {$firstTeamName} & {$secondTeamName} match";
            $data['title'] = $title;
            $data['description'] = $body;
        } elseif ($pred->status == 2) {
            $title = "Winner prediction";
            $body = "You win {$pred->points} points from {$firstTeamName} & {$secondTeamName} match";
            $data['title'] = $title;
            $data['description'] = $body;
        } else {
            $title = "Wrong prediction";
            $body = "You lost 10 points from {$firstTeamName} & {$secondTeamName} match";
            $data['title'] = $title;
            $data['description'] = $body;
        }
        $notification->data = array_merge($notification->data, $data);
        $notification->save();
    }
});


Route::get('/notify/{type?}', "NotificationController@notify");

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/job', function () {
    \Illuminate\Support\Facades\Queue::push(new App\Jobs\JustForTestingJob("Mohammed"));
});

Route::get('/jobf', function () {
    \Illuminate\Support\Facades\Queue::push(new App\Jobs\JustForTestingJob("F"));
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
