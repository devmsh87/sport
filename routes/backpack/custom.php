<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    CRUD::resource('association', 'AssociationCrudController');
    CRUD::resource('country', 'CountryCrudController');
    CRUD::resource('favorite', 'FavoriteCrudController');
    CRUD::resource('league', 'LeagueCrudController');
    CRUD::resource('match', 'MatchCrudController');
    CRUD::resource('matchresultprediction', 'MatchResultPredictionCrudController');
    CRUD::resource('matchteam', 'MatchTeamCrudController');
    CRUD::resource('point', 'PointCrudController');
    CRUD::resource('prediction', 'PredictionCrudController');
    CRUD::resource('season', 'SeasonCrudController');
    CRUD::resource('team', 'TeamCrudController');
    CRUD::resource('user', 'UserCrudController');
    CRUD::resource('usermatchresult', 'UserMatchResultCrudController');
}); // this should be the absolute last line of this file
