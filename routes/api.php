<?php

use App\Notifications\FCMNotification;
use App\User;
use App\Test;
use Carbon\Carbon;
use LaravelFCM\Facades\FCMGroup;
use sngrl\PhpFirebaseCloudMessaging\Client;

Route::get('guide', 'API\UserController@guide');

Route::post('users/login', 'API\UserController@login');

Route::resource('leagues', 'API\LeagueController');
Route::resource('teams', 'API\TeamController');
Route::resource('seasons', 'API\SeasonController');
Route::resource('seasons.groups', 'API\SeasonGroupController');
Route::resource('seasons.stages', 'API\SeasonStageController');
Route::get('/leaderboard', 'API\UserController@leaderboard');
Route::get('predictions/{id}/image/{x}/{y}', 'API\PredictionController@image');
Route::get('predictions/{id}/image16/{time}', 'API\PredictionController@image16');
Route::get('matches/best', 'API\MatchController@best');
Route::resource('matches', 'API\MatchController');
Route::resource('stages.predictions', 'API\StagePredictionController');
Route::get('users/{user}', 'API\UserController@show');

Route::middleware('auth:api')->group(function () {
    Route::post('suggestions/{type}', 'API\SuggestionController@store');
    Route::post('friends/{user}', 'API\UserController@follow');
    Route::get('me', 'API\UserController@me');
    Route::get('me/complete', 'API\UserController@complete');
    Route::get('notifications', 'API\UserController@notifications');

    Route::resource('favorites', 'API\FavoriteController');

    Route::resource('matches.results', 'API\MatchResultController');

    Route::get('predictions/matches', 'API\PredictionController@matches');
    Route::get('predictions/{season}/stages', 'API\PredictionController@stages');
    Route::resource('matches.resultPredictions', 'API\MatchResultPredictionController');
    Route::resource('matches.teamPredictions', 'API\MatchTeamPredictionController');
});

// JUST FOR TESTING PURPOSES

Route::get('friends', 'API\UserController@friends');



Route::get('/evaluate/{id}/{first}/{second}', function ($id,$first,$second) {
    $match = \App\Match::findOrFail($id);
    $match->finish($first,$second);
    $match = $match->fresh();
    return $match->evaluate();
});

Route::get('/addGroup', function () {
    $tokens = [
        "fbVvRPEpnps:APA91bGwDlLClPhPwSYV9pkev_Ly1P9SUb1X0xRQI7V0eTquDCYlXAroCatxeueMvYAdvDG9aCaLcV8dgnRvcu1J2RSDx_hJx2A8AhpT8xRJc8ioQGbrwMqnYFjuiq2Mz24CqIKXXB1x"
    ];
    $groupName = "team_1";

    $notificationKey = FCMGroup::createGroup($groupName, $tokens);
    dd($notificationKey);
});

Route::get('/addToGroup', function () {
    $tokens = [
        "123123fbVvRPEpnps:APA91bGwDlLClPhPwSYV9pkev_Ly1P9SUb1X0xRQI7V0eTquDCYlXAroCatxeueMvYAdvDG9aCaLcV8dgnRvcu1J2RSDx_hJx2A8AhpT8xRJc8ioQGbrwMqnYFjuiq2Mz24CqIKXXB1x"
    ];
    $groupName = "team_1";
    $notificationKey = "APA91bFSv4H7lVAVFPpdCQ2oyaSlqLqWdJAd93OZDUQjq_jTOwr_750-mSmsMH6QFENK-y9OwGkPiNNsZVuE6KmV0Iz9prJCKFybkA3E3bP5o2IRKEWmWIA";
    FCMGroup::addToGroup($groupName, $notificationKey, $tokens);
});

Route::get('/group', function () {
    $notificationKey = "APA91bFSv4H7lVAVFPpdCQ2oyaSlqLqWdJAd93OZDUQjq_jTOwr_750-mSmsMH6QFENK-y9OwGkPiNNsZVuE6KmV0Iz9prJCKFybkA3E3bP5o2IRKEWmWIA";
    /** @var \LaravelFCM\Response\GroupResponse $response */
    $response = FCMNotification::notifyGroup($notificationKey, "title group", "body", [
        'key' => 'val'
    ]);

    return [
        $response->numberSuccess(),
        $response->numberFailure(),
        $response->tokensFailed(),
    ];
});

Route::get('/addTopic', function () {
    $server_key = env('FCM_SERVER_KEY');
    $client = new Client();
    $client->setApiKey($server_key);
    $client->injectGuzzleHttpClient(new \GuzzleHttp\Client());

    $tokens = [
        "ekVXqj4d428:APA91bGRyyg_qOaHGAkLs1xXJ-yqCUXflhX6k3qzHYaQuQuNY4AfavysxZ5h8sIPhcm6nANCO5j7bNQN1-yroEl0_6QyjK1Ghv6ywalEDxGsOpi_R2OUw0WfeprtmltyCNtx2Rd0nP3b"
    ];
    $response = $client->addTopicSubscription('All', $tokens);
    var_dump($response->getStatusCode());
    var_dump($response->getBody()->getContents());

});

Route::get('/topic', function () {
    /** @var \LaravelFCM\Response\TopicResponse $response */
    $response = FCMNotification::notifyTopic('all', "title 111", "body", [
        'key' => 'val'
    ]);

    return [
        $response->isSuccess(),
    ];
});

Route::get('/topic1', function () {
    /** @var \LaravelFCM\Response\TopicResponse $response */
    $response = FCMNotification::notifyTopic('all-1', "title 111", "body", [
        'key' => 'val'
    ]);

    return [
        $response->isSuccess(),
    ];
});


Route::get('/notify', function () {

    $tokens = User::whereNotNull('device_token')->pluck('device_token');

    /** @var \LaravelFCM\Response\DownstreamResponse $response */
    $response = FCMNotification::notify($tokens, "World Cup 2018!", "Predict now");

    return [
        $response->tokensToDelete(),
        $response->tokensToModify(),
        $response->hasMissingToken(),
    ];
});

Route::get('/snotify', function () {

    $tokens = User::whereNotNull('device_token')->pluck('device_token');
    $tokens = $tokens->all();

    /** @var \LaravelFCM\Response\DownstreamResponse $response */
    $response = FCMNotification::notify($tokens, null, null, [
        'type' => 'delete_cache',
        'model' => 'teams'
    ]);

    return [
        $response->tokensToDelete(),
        $response->tokensToModify(),
        $response->hasMissingToken(),
    ];
});

Route::get('/notifications1', function () {
    $notifications = factory(\Illuminate\Notifications\DatabaseNotification::class, 10)->make();

    $i = 1;
    foreach ($notifications as $notification) {
        $notification->id = $i++;
    }

    $notifications = $notifications->map(function ($notification) {
        foreach ($notification->data as $key => $value) {
            $notification->$key = $value;
        }
        unset($notification->data);
        $notification->is_read = !!rand(0, 1);
        $notification->created_at = Carbon::now();
        return $notification;
    });

    return $notifications->toArray();
});

Route::get('/mongo', function () {
    $test = new Test();
    $test->qwe = "vasadasd";
    $test->save();

    return $test;
});