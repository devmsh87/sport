<?php

namespace Tests\Feature;

use App\Match;
use Carbon\Carbon;
use Tests\TestCase;

class MatchesTest extends TestCase
{
    public function test_user_can_get_matches()
    {
        $this->createMatch(Carbon::yesterday());
        $this->createMatch(Carbon::now());
        $this->createMatch(Carbon::tomorrow());

        $response = $this->get('api/matches');

        $response->assertJsonStructure($this->matchesFields());
    }

    public function test_user_can_get_matches_for_specific_date()
    {
        $this->createMatch(Carbon::yesterday());
        $this->createMatch(Carbon::now());
        $this->createMatch(Carbon::tomorrow());

        $response = $this->get('api/matches?date=' . Carbon::now()->format("d-m-Y"));

        $response->assertJsonStructure($this->matchesFields());
    }

    public function test_user_can_get_matches_for_specific_league()
    {
        $this->createMatch(Carbon::yesterday());
        $this->createMatch(Carbon::now());
        $this->createMatch(Carbon::tomorrow());

        $response = $this->get('api/matches?league_id=1');

        $response->assertJsonStructure($this->matchesFields());
    }

    public function test_user_can_get_matches_for_specific_team()
    {
        $this->createMatch(Carbon::yesterday());
        $this->createMatch(Carbon::now());
        $this->createMatch(Carbon::tomorrow());

        $response = $this->get('api/matches?team_id=1');

        $response->assertJsonStructure($this->matchesFields());
    }

    public function test_user_can_get_matches_for_specific_season()
    {
        $this->createMatch(Carbon::yesterday());
        $this->createMatch(Carbon::now());
        $this->createMatch(Carbon::tomorrow());

        $response = $this->get('api/matches?season_id=1');

        $response->assertJsonStructure($this->matchesFields());
    }

    public function test_user_cant_get_matches_with_invalid_data()
    {
        $this->createMatch(Carbon::yesterday());
        $this->createMatch(Carbon::now());
        $this->createMatch(Carbon::tomorrow());

        $response = $this->get('api/matches?league_id=10&team_id=10&season_id=10&date=xxx', ['Accept' => 'application/json']);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['league_id', 'team_id', 'season_id', 'date']);
    }

    public function test_user_can_get_match_details()
    {
        $this->createMatch();

        $response = $this->get('api/matches/1');

        $response->assertJsonStructure($this->matchFields());
    }

    public function test_match_can_be_live()
    {
        $test_times = [
            0, 20, 50, 80, 105
        ];

        foreach ($test_times as $key => $time) {
            $id = $key + 1;
            $this->createMatch(Carbon::now()->subMinutes($time));
            $response = $this->get("api/matches/{$id}");
            $response->assertJson(['time_status' => 'Live']);
        }
    }

    public function test_match_can_be_scheduled()
    {
        $this->createMatch(Carbon::now()->addDay());

        $response = $this->get('api/matches/1');

        $response->assertJson(['time_status' => 'Scheduled']);
    }

    public function test_match_can_be_finished()
    {
        $this->createMatch(Match::markAsFinished());

        $response = $this->get('api/matches/1');

        $response->assertJson(['time_status' => 'Finished']);
    }

    /**
     * @return array
     */
    public function matchFields()
    {
        return [
            'id',
            'status',
            'first_team_id',
            'first_team_name',
            'first_team_logo',
            'second_team_id',
            'second_team_name',
            'second_team_logo',
        ];
    }

    /**
     * @return array
     */
    public function matchesFields(): array
    {
        return [
            [
                'date',
                'leagues' => [
                    [
                        'id',
                        'name',
                        'logo',
                        'matches' => [
                            $this->matchFields()
                        ]
                    ]
                ]
            ]
        ];
    }
}
