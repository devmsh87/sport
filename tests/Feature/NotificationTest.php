<?php

namespace Tests\Feature;

use App\Notifications\FCMNotification;
use App\User;
use Illuminate\Support\Collection;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Message\Topics;
use LaravelFCM\Response\DownstreamResponse;
use LaravelFCM\Response\GroupResponse;
use LaravelFCM\Response\TopicResponse;
use Mockery;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NotificationTest extends TestCase
{
    public function test_can_get_notifications()
    {
        $this->loginAsNewUser();
        $response = $this->get('api/notifications');

        $response->assertJsonStructure([
            [
                'id',
                'icon',
                'title',
                'description',
                'is_read',
                'created_at',
            ]
        ]);
    }

    public function test_can_send_notification_to_multi_devices()
    {
        $this->markTestSkipped("chuck problem");
        $numberSuccess = 2;
        $mockResponse = new \LaravelFCM\Mocks\MockDownstreamResponse($numberSuccess);

        $mockResponse->addTokenToDelete('token_to_delete');
        $mockResponse->addTokenToModify('token_to_modify', 'token_modified');
        $mockResponse->setMissingToken(true);

        $sender = Mockery::mock(\LaravelFCM\Sender\FCMSender::class);
        $sender->shouldReceive('sendTo')->once()->andReturn($mockResponse);

        $this->app->singleton('fcm.sender', function($app) use($sender) {
            return $sender;
        });

        $tokens = new Collection();
        $title = 'my title';
        $body = 'Hello world';
        $data = ['a_data' => 'my_data'];

        /** @var DownstreamResponse $response */
        $response = FCMNotification::notify($tokens,$title,$body,$data);

        $this->assertEquals(3,$response->numberSuccess());
        $this->assertCount(1,$response->tokensToDelete());
        $this->assertCount(1,$response->tokensToModify());
        $this->assertCount(0,$response->tokensWithError());
    }

    public function test_can_send_notification_to_topic()
    {
        $mockResponse = new \LaravelFCM\Mocks\MockTopicResponse();
        $mockResponse->setSuccess("123");

        $sender = Mockery::mock(\LaravelFCM\Sender\FCMSender::class);
        $sender->shouldReceive('sendToTopic')->once()->andReturn($mockResponse);

        $this->app->singleton('fcm.sender', function($app) use($sender) {
            return $sender;
        });

        $topic = 'topic';
        $title = 'my title';
        $body = 'Hello world';
        $data = ['a_data' => 'my_data'];

        /** @var TopicResponse $response */
        $response = FCMNotification::notifyTopic($topic,$title,$body,$data);

        $this->assertEquals(true,$response->isSuccess());
    }

    public function test_can_send_notification_to_group()
    {
        $mockResponse = new \LaravelFCM\Mocks\MockGroupResponse();
        $mockResponse->setNumberSuccess(5);

        $sender = Mockery::mock(\LaravelFCM\Sender\FCMSender::class);
        $sender->shouldReceive('sendToGroup')->once()->andReturn($mockResponse);

        $this->app->singleton('fcm.sender', function($app) use($sender) {
            return $sender;
        });

        $group = ['group'];
        $title = 'my title';
        $body = 'Hello world';
        $data = ['a_data' => 'my_data'];

        /** @var GroupResponse $response */
        $response = FCMNotification::notifyGroup($group,$title,$body,$data);

        $this->assertEquals(5,$response->numberSuccess());
    }

    public function test_cannot_send_notification_to_more_than_1000_devices()
    {
        $this->markTestSkipped("we did not throw exception anymore, we must test the chunk");
        $tokens = collect(range(0,1001));
        $title = 'my title';
        $body = 'Hello world';
        $data = ['a_data' => 'my_data'];

        $this->expectException(\Exception::class);

        FCMNotification::notify($tokens,$title,$body,$data);
    }
}
