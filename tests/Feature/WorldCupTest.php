<?php

namespace Tests\Feature;

use App\Match;
use App\MatchTeam;
use App\MatchTeamPrediction;
use App\Season;
use App\Stage;
use App\StagePrediction;
use Tests\TestCase;

class WorldCupTest extends TestCase
{
    public function test_anyone_can_get_world_cup_groups()
    {
        $season = $this->createGroup(8);
        $response = $this->get("api/seasons/{$season->id}/groups");

        $response->assertSuccessful();
        $this->assertCount(8, $response->json());
        $response->assertJsonStructure([
            [
                "name",
                "teams" => [
                    [
                        "id",
                        "name",
                        "logo"
                    ]
                ]
            ]
        ]);
    }

    public function test_can_get_world_cup_stages_list()
    {
        $this->withoutExceptionHandling();

        $worldCup = $this->createWorldCupStages();

        $response = $this->get("api/seasons/{$worldCup->id}/stages");

        $response->assertSuccessful();
        $this->assertCount(6, $response->json());
        $response->assertJsonStructure([
            [
                "id",
                "name",
            ]
        ]);
    }

    public function test_can_add_stage_16_matches_with_fake_teams()
    {
        $matchTeam = factory(Match::class)->create([
            'first_team_label' => 'A1',
            'second_team_label' => 'B2',
            'first_team_id' => null,
            'second_team_id' => null,
            'stage_id' => factory(Stage::class)->create([
                'name' => '16'
            ])->id
        ]);

        $this->assertEquals('A1', $matchTeam->first_team_label);
        $this->assertEquals('B2', $matchTeam->second_team_label);
        $this->assertNull($matchTeam->first_team_id);
        $this->assertNull($matchTeam->second_team_id);

        $this->assertEquals('16', $matchTeam->stage->name);
    }

    public function test_add_all_world_cup_matches()
    {
        $this->markTestSkipped("No need for now!");
        $worldCup = factory(Season::class)->create();

        $groups = $this->createGroup(8, $worldCup);

        $stages = $this->createWorldCupStages($worldCup);

        $this->createWorldCupMatches($worldCup);
    }

    public function test_math()
    {
        $this->markTestSkipped("not a real test, just take a look about the non leaner calculation");
        $percents = collect([
            // [success, before]
            [100, 100],
            [90, 100],
            [80, 100],
            [70, 100],
            [60, 100],
            [50, 100],
            [40, 100],
            [30, 100],
            [20, 100],
            [10, 100],
            [100, 50],
        ]);

        $results = $percents->map(function ($group) {
            $values = collect([$group[0], $group[1]]);

            $percent = $values->avg();

            $percent = pow($percent, 3);

            $percent = $this->map($percent, 0, pow(100, 3), 0, 100);

            return $percent;
        });

        dd($results);
    }

    private function map($value, $fromLow, $fromHigh, $toLow, $toHigh)
    {
        $fromRange = $fromHigh - $fromLow;
        $toRange = $toHigh - $toLow;
        $scaleFactor = $toRange / $fromRange;

        // Re-zero the value within the from range
        $tmpValue = $value - $fromLow;
        // Rescale the value to the to range
        $tmpValue *= $scaleFactor;
        // Re-zero back to the to range
        return $tmpValue + $toLow;
    }
}
