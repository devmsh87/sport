<?php

namespace Tests\Feature;

use App\Match;
use App\MatchResultPrediction;
use App\MatchTeamPrediction;
use App\Stage;
use App\StagePrediction;
use App\User;
use Carbon\Carbon;
use Tests\TestCase;


class MatchResultPredictionTest extends TestCase
{
    public function test_user_predict_match_result()
    {
        $this->loginAsNewUser();
        $match = $this->createMatch();

        $response = $this->postJson("api/matches/{$match->id}/resultPredictions", [
            'first_team_result' => 1,
            'second_team_result' => 2,
        ]);

        $response = $this->postJson("api/matches/{$match->id}/resultPredictions", [
            'first_team_result' => 2,
            'second_team_result' => 4,
        ]);

        $response->assertSuccessful();
        $response->assertJsonStructure([
            'id',
            'match_id',
            'first_team_result',
            'second_team_result',
        ]);

        $mrp = MatchResultPrediction::find(1);
        $this->assertEquals(2,$mrp->first_team_result);
        $this->assertEquals(4,$mrp->second_team_result);
    }

    public function test_user_cant_predict_invalid_match()
    {
        $this->loginAsNewUser();

        $response = $this->postJson("api/matches/99/resultPredictions", [
            'first_team_result' => 1,
            'second_team_result' => 2,
        ]);

        $response->assertStatus(404);
    }

    public function test_user_cant_predict_without_enough_points()
    {
        $user = $this->loginAsNewUser();
        $user->update(['points' => 0]);

        $match = $this->createMatch();
        $response = $this->postJson("api/matches/{$match->id}/resultPredictions", [
            'first_team_result' => 1,
            'second_team_result' => 2,
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['match']);
    }

    public function test_guest_cant_predict_match_result()
    {
        $match = $this->createMatch();

        $response = $this->postJson("api/matches/{$match->id}/resultPredictions", [
            'first_team_result' => 1,
            'second_team_result' => 2,
        ]);

        $response->assertStatus(401);
    }

    public function test_user_cant_predict_started_match_result()
    {
        $this->loginAsNewUser();
        $match = $this->createMatch(Carbon::yesterday());

        $response = $this->postJson("api/matches/{$match->id}/resultPredictions", [
            'first_team_result' => 1,
            'second_team_result' => 2,
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['match']);
    }

    public function test_user_cant_predict_matches_will_start_after_two_weeks()
    {
        $this->loginAsNewUser();
        $match = $this->createMatch(Carbon::tomorrow()->addMonth());

        $response = $this->postJson("api/matches/{$match->id}/resultPredictions", [
            'first_team_result' => 1,
            'second_team_result' => 2,
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['match']);
    }

    public function test_user_add_match_result()
    {
        $this->loginAsNewUser();
        $match = $this->createMatch(Carbon::yesterday());

        $response = $this->postJson("api/matches/{$match->id}/results", [
            'first_team_result' => 1,
            'second_team_result' => 2,
        ]);

        $response->assertJsonStructure([
            'id',
            'match_id',
            'first_team_result',
            'second_team_result',
        ]);
    }

    public function test_user_cant_add_match_result_before_match_finish()
    {
        $this->loginAsNewUser();
        $match = $this->createMatch(Carbon::tomorrow());

        $response = $this->postJson("api/matches/{$match->id}/results", [
            'first_team_result' => 1,
            'second_team_result' => 2,
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['match']);
    }

    public function test_user_can_get_his_predictions()
    {
        $this->loginAsNewUser();

        $this->createMatch(Carbon::tomorrow()->addDay())->predict(3, 4);
        $this->createMatch(Carbon::tomorrow())->predict(1, 2);

        $stage = $this->createStage16Matches();

        StagePrediction::predict([$stage, [
            "A1" => 1, "A2" => 2, "B1" => 3, "B2" => 4, "C1" => 5, "C2" => 6, "D1" => 7, "D2" => 8,
            "E1" => 9, "E2" => 10, "F1" => 11, "F2" => 12, "G1" => 13, "G2" => 14, "H1" => 15, "H2" => 16,
        ],""]);

        $this->assertEquals(1,StagePrediction::count());
        $this->assertEquals(8,MatchTeamPrediction::count());
    }
}
