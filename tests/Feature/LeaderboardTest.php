<?php

namespace Tests\Feature;

use App\MatchResultPrediction;
use App\User;
use Tests\TestCase;

class LeaderboardTest extends TestCase
{
    public function test_can_get_leaderboard()
    {
        // we must have evaluated predictions to appear in the leaderboard
        factory(MatchResultPrediction::class)->create(['status' => 2]);

        $response = $this->get('api/leaderboard');

        $response->assertJsonStructure([
            [
                'id',
                'name',
                'photo',
                'rank',
                'points',
            ]
        ]);
    }
}
