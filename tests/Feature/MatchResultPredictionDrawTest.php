<?php

namespace Tests\Feature;

use App\Match;
use App\MatchResultPrediction;
use App\MatchTeamPrediction;
use App\Stage;
use App\StagePrediction;
use App\User;
use Carbon\Carbon;
use Tests\TestCase;


class MatchResultPredictionDrawTest extends TestCase
{
    public function test_user_cant_predict_draw_in_non_draw_stage()
    {
        $match = $this->createMatch();
        $match->stage->update([
            'can_draw' => 0
        ]);

        $this->loginAsNewUser();
        $response = $this->postJson("api/matches/{$match->id}/resultPredictions", [
            'first_team_result' => 1,
            'second_team_result' => 1,
            'position' => null,
        ]);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors(['match']);
    }

    public function test_user_can_predict_draw_and_position_in_non_draw_stage()
    {
        $match = $this->createMatch();
        $match->stage->update([
            'can_draw' => 0
        ]);

        $this->loginAsNewUser();
        $response = $this->postJson("api/matches/{$match->id}/resultPredictions", [
            'first_team_result' => 1,
            'second_team_result' => 1,
            'position' => Match::POSITION_SECOND,
        ]);

        $mrp = MatchResultPrediction::find(1);

        $response->assertSuccessful();
        $this->assertEquals(Match::POSITION_SECOND, $mrp->position);

        $response = $this->get('api/matches/1');
        $this->assertCount(0, $response->json('stats.first_team_friends'));
        $this->assertCount(1, $response->json('stats.second_team_friends'));
        $this->assertCount(0, $response->json('stats.draw_friends'));
    }

    public function test_user_can_predict_draw_and_position_in_draw_stage()
    {
        $match = $this->createMatch();
        $match->stage->update([
            'can_draw' => 1
        ]);

        $this->loginAsNewUser();
        $response = $this->postJson("api/matches/{$match->id}/resultPredictions", [
            'first_team_result' => 1,
            'second_team_result' => 1,
            'position' => null,
        ]);

        $mrp = MatchResultPrediction::find(1);

        $response->assertSuccessful();
        $this->assertNull($mrp->position);

        $response = $this->get('api/matches/1');
        $this->assertCount(0, $response->json('stats.first_team_friends'));
        $this->assertCount(0, $response->json('stats.second_team_friends'));
        $this->assertCount(1, $response->json('stats.draw_friends'));
    }

    public function test_jackpot_algorithms_with_draw_for_non_draw_match()
    {
        $match = $this->createMatch();
        $match->stage->update([
            'can_draw' => false
        ]);

        $this->loginAsNewUser();
        $exactPrediction = $match->predict(1, 2);

        $this->loginAsNewUser();
        $losePrediction = $match->predict(2, 1);

        $this->loginAsNewUser();
        $diffPrediction = $match->predict(2, 3);

        $this->loginAsNewUser();
        $winnerPrediction = $match->predict(2, 2, Match::POSITION_SECOND);

        $match->started_at = Match::markAsFinished();
        $match->finish(1, 2);
        $match->evaluate();

        $this->assertEquals(21.333333333334, $exactPrediction->fresh()->points);
        $this->assertEquals(16.0000000000005, $diffPrediction->fresh()->points);
        $this->assertEquals(10.666666666667, $winnerPrediction->fresh()->points);
        $this->assertEquals(-10.0, $losePrediction->fresh()->points);
    }

    public function test_jackpot_algorithms_with_draw_for_draw_match()
    {
        $match = $this->createMatch();
        $match->stage->update([
            'can_draw' => false
        ]);

        $this->loginAsNewUser();
        $exactPrediction = $match->predict(2, 2, Match::POSITION_SECOND);

        $this->loginAsNewUser();
        $losePrediction = $match->predict(2, 1);

        $this->loginAsNewUser();
        $diffPrediction = $match->predict(3, 3, Match::POSITION_SECOND);

        $this->loginAsNewUser();
        $winnerPrediction = $match->predict(1, 2);

        $match->started_at = Match::markAsFinished();
        $match->finish(2, 2, Match::POSITION_SECOND);
        $match->evaluate();

        $this->assertEquals(21.333333333334, $exactPrediction->fresh()->points);
        $this->assertEquals(16.0000000000005, $diffPrediction->fresh()->points);
        $this->assertEquals(10.666666666667, $winnerPrediction->fresh()->points);
        $this->assertEquals(-10, $losePrediction->fresh()->points);
    }

}
